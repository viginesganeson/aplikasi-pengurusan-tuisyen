package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.DayService;

import java.util.ArrayList;
import java.util.List;

public class AdminViewClassByDay extends AppCompatActivity {

    private Toolbar toolbar;
    private String day;
    private FloatingActionButton floatingActionButton;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Class> classList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_class_by_day);

        Intent intent = getIntent();
        day = intent.getStringExtra("DAY");

        toolbar = findViewById(R.id.tlb_view_class_by_day);
        recyclerView = findViewById(R.id.rcview_admin_view_class_by_Day);
        linearLayoutManager = new LinearLayoutManager(AdminViewClassByDay.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = findViewById(R.id.tv_empty_class);
        floatingActionButton = findViewById(R.id.fab_add_class);

        floatingActionButton.setOnClickListener(this.addClass);
        setToolbar(toolbar, day);

    }

    private View.OnClickListener addClass = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(AdminViewClassByDay.this, AdminAddClass.class);
            intent.putExtra("DAY", day);
            startActivity(intent);
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar, String day){
        toolbar.setTitle(day);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadClass();
    }

    private void loadClass(){
        classList = new ArrayList<>();
        new ClassService().getClassByDay(AdminViewClassByDay.this, day, classList, textView, recyclerView);
    }
}
