package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Fee;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FeeService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.NoteService;

import java.util.List;

public class AdminViewStudentPaymentListAdapter extends RecyclerView.Adapter<AdminViewStudentPaymentListAdapter.AdminViewStudentPaymentListViewHolder> {

    private List<Fee> feeList;
    private Context context;
    private CoordinatorLayout coordinatorLayout;

    public AdminViewStudentPaymentListAdapter(Context context, List<Fee> feeList, CoordinatorLayout coordinatorLayout){
        this.context = context;
        this.feeList = feeList;
        this.coordinatorLayout = coordinatorLayout;
    }

    @NonNull
    @Override
    public AdminViewStudentPaymentListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_payment,null);
        return new AdminViewStudentPaymentListAdapter.AdminViewStudentPaymentListViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdminViewStudentPaymentListViewHolder adminViewStudentPaymentListViewHolder, final int i) {
        adminViewStudentPaymentListViewHolder.tvYear.setText(feeList.get(i).getYear());
        adminViewStudentPaymentListViewHolder.tvFeeId.setText(feeList.get(i).getId());
        adminViewStudentPaymentListViewHolder.tvMonth.setText(feeList.get(i).getMonth());
        final String BUTTON_TEXT;

        if (feeList.get(i).getStatus()){
            adminViewStudentPaymentListViewHolder.tvStatusApproved.setVisibility(View.VISIBLE);
            BUTTON_TEXT = "UNAPPROVED";
        } else {
            adminViewStudentPaymentListViewHolder.tvStatusUnapproved.setVisibility(View.VISIBLE);
            BUTTON_TEXT = "APRROVE";
        }

        adminViewStudentPaymentListViewHolder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder
                        .setMessage("Student Payments")
                        .setTitle("Students Payments")
                        .setPositiveButton(BUTTON_TEXT, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (feeList.get(i).getStatus()){
                                    new FeeService().updateFee(context, feeList.get(i).getId(), coordinatorLayout, false);
                                } else {
                                    new FeeService().updateFee(context, feeList.get(i).getId(), coordinatorLayout, true);
                                }

                            }
                        })
                        .setNeutralButton("Download", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                                Uri uri = Uri.parse(feeList.get(i).getFeeUrl());
                                DownloadManager.Request request = new DownloadManager.Request(uri);
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                                request.setDestinationInExternalPublicDir( "/", feeList.get(i).getId() + ".pdf");
                                downloadManager.enqueue(request);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();

                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return feeList.size();
    }

    public class AdminViewStudentPaymentListViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMonth, tvYear, tvFeeId, tvStatusUnapproved, tvStatusApproved;
        private CardView cardView;
        public AdminViewStudentPaymentListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            tvFeeId = itemView.findViewById(R.id.tv_payment_id);
            cardView = itemView.findViewById(R.id.rcview_view_payment_cview);
            tvStatusUnapproved = itemView.findViewById(R.id.tv_status_unapproved);
            tvStatusApproved = itemView.findViewById(R.id.tv_status_approved);
        }
    }
}
