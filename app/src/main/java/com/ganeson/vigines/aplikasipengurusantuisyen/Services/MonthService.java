package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminViewMonthsForStudentPaymentAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminViewMonthsForTeacherPaymentAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.StudentViewMonthsForPaymentAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewMonthsForPaymentAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MonthService {

    private DatabaseReference databaseReference;
    private Month month;
    private String [] months;
    private Integer NUMBER_OF_MONTHS = 12;

    public MonthService () {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_MONTH);
    }

    private void addMonth(){
        months = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        for (int i = 0 ; i < NUMBER_OF_MONTHS; i++){
            String id = databaseReference.push().getKey();
            Month day = new Month(id, months[i]);
            if (id != null) {
                databaseReference.child(id).setValue(day)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            System.out.println(e.getMessage());
                        }
                    });
            }
        }
    }

    public void checkIfExist() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    addMonth();
                } else {
                    System.out.println("Months is available");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getAllMonths(final Context context, final List<Month> monthList, final RecyclerView recyclerView, final Integer userType, final String SOURCE_TYPE, final String USER_ID) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot monthDataSnapshot : dataSnapshot.getChildren()) {
                        month = monthDataSnapshot.getValue(Month.class);
                        monthList.add(month);
                    }
                }

                if(userType.equals(Constant.TYPE_STUDENT)){
                    recyclerView.setAdapter(new StudentViewMonthsForPaymentAdapter(context, monthList));
                }

                if(userType.equals(Constant.TYPE_TEACHER)) {
                    recyclerView.setAdapter(new TeacherViewMonthsForPaymentAdapter(context, monthList));
                }

                if(userType.equals(Constant.TYPE_ADMIN)) {
                    if(SOURCE_TYPE.equals("ADMIN_VIEW_TEACHER_PAYMENT")){
                        recyclerView.setAdapter(new AdminViewMonthsForTeacherPaymentAdapter(context, monthList, USER_ID));
                    } else if (SOURCE_TYPE.equals("ADMIN_VIEW_STUDENT_PAYMENT")) {
                        recyclerView.setAdapter(new AdminViewMonthsForStudentPaymentAdapter(context, monthList, USER_ID));
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
