package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Salary {

    String id, year, month, teacherId, salaryUrl;
    Boolean status;

    public Salary () {}

    public Salary(String id, String year, String month, String teacherId, String salaryUrl, Boolean status) {
        this.id = id;
        this.year = year;
        this.month = month;
        this.teacherId = teacherId;
        this.salaryUrl = salaryUrl;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getSalaryUrl() {
        return salaryUrl;
    }

    public void setSalaryUrl(String salaryUrl) {
        this.salaryUrl = salaryUrl;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
