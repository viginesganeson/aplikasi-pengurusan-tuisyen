package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.ganeson.vigines.aplikasipengurusantuisyen.Auth.Login;
import com.ganeson.vigines.aplikasipengurusantuisyen.Home;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SubjectService;
import com.google.firebase.auth.FirebaseAuth;

import io.paperdb.Paper;

public class AdminViewSubject extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btnUpdate;
    private TextInputEditText txtName, txtCode, txtDesc;
    private CoordinatorLayout coordinatorLayout;
    private RelativeLayout relativeLayout;
    private String SUBJECT_ID, SUBJECT_NAME, SUBJECT_CODE, SUBJECT_DESC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_subject);

        toolbar = findViewById(R.id.tlb_view_subject);
        coordinatorLayout = findViewById(R.id.cdlayout_admin_view_subject);
        relativeLayout = findViewById(R.id.rlayout_admin_view_subject);
        btnUpdate = findViewById(R.id.btn_update_subject);
        txtName = findViewById(R.id.txt_subject_name);
        txtCode = findViewById(R.id.txt_subject_code);
        txtDesc = findViewById(R.id.txt_subject_desc);

        btnUpdate.setOnClickListener(this.updateSubject);
        setToolbar(toolbar);
        setData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.admin_view_subject, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete_subject) {
            showDeleteDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminViewSubject.this);
        builder.setMessage("Are you sure to delete").setTitle("Delete");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                new SubjectService().deleteSubject(SUBJECT_ID, AdminViewSubject.this, coordinatorLayout);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // GET DATA FROM PREVIOUS ACT
    private void setData(){
        Intent intent = getIntent();
        SUBJECT_CODE = intent.getStringExtra("SUBJECT_CODE");
        SUBJECT_ID = intent.getStringExtra("SUBJECT_ID");
        SUBJECT_NAME = intent.getStringExtra("SUBJECT_NAME");
        SUBJECT_DESC = intent.getStringExtra("SUBJECT_DESC");

        txtDesc.setText(SUBJECT_DESC);
        txtCode.setText(SUBJECT_CODE);
        txtName.setText(SUBJECT_NAME);
    }

    private View.OnClickListener updateSubject = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);

            String name = null;
            String desc = null;
            String code = null;

            if (TextUtils.isEmpty(txtName.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout,"Name Required");
                txtName.requestFocus();
            } else if (TextUtils.isEmpty(txtCode.getText())){
                new GlobalService().showSnackBar(coordinatorLayout,"Code Required");
                txtCode.requestFocus();
            } else {
                name = txtName.getText().toString();
                desc = txtDesc.getText().toString();
                code = txtCode.getText().toString();
                new SubjectService().updateSubject(AdminViewSubject.this, SUBJECT_ID, coordinatorLayout, code, name, desc);
            }
        }
    };
}
