package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Note;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.snapshot.DoubleNode;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.StorageReference;


import java.util.List;

public class StudentViewNotesToDownloadAdapter extends RecyclerView.Adapter<StudentViewNotesToDownloadAdapter.StudentViewNotesToDownloadViewHolder> {

    private List<Note> noteList;
    private Context context;
    private CoordinatorLayout coordinatorLayout;

    public StudentViewNotesToDownloadAdapter(Context context, List<Note> noteList, CoordinatorLayout coordinatorLayout){
        this.context = context;
        this.noteList = noteList;
        this.coordinatorLayout = coordinatorLayout;
    }

    @NonNull
    @Override
    public StudentViewNotesToDownloadViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_student_view_notes_to_download,null);
        StudentViewNotesToDownloadAdapter.StudentViewNotesToDownloadViewHolder crsv = new StudentViewNotesToDownloadAdapter.StudentViewNotesToDownloadViewHolder(layoutView);
        return crsv;
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewNotesToDownloadViewHolder studentViewNotesToDownloadViewHolder, final int i) {
        studentViewNotesToDownloadViewHolder.textView.setText(noteList.get(i).getId());
        studentViewNotesToDownloadViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(noteList.get(i).noteUrl);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir( "/", noteList.get(i).id + ".pdf");
                downloadManager.enqueue(request);
            }
        });
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    public class StudentViewNotesToDownloadViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private TextView textView;

        public StudentViewNotesToDownloadViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.rcview_student_view_notes_to_download_cview);
            textView = itemView.findViewById(R.id.tv_note_id);
        }
    }
}
