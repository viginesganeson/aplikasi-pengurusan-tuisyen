package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.MonthService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherPayment;

import java.util.ArrayList;
import java.util.List;

public class AdminViewTeacherSalaryByMonth extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<Month> monthList;
    private Toolbar toolbar;
    private String TEACHER_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TEACHER_ID = getIntent().getStringExtra("USER_ID");
        setContentView(R.layout.activity_admin_view_teacher_salary_by_month);
        toolbar = findViewById(R.id.tlb_teacher_view_salary_by_month);
        recyclerView = findViewById(R.id.rcview_teacher_view_salary_by_month);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        setToolbar(toolbar);
    }

    private void loadMonths(){
        monthList = new ArrayList<>();
        new MonthService().getAllMonths(getApplicationContext(), monthList, recyclerView, Constant.TYPE_ADMIN, "ADMIN_VIEW_TEACHER_PAYMENT", TEACHER_ID);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMonths();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("View Payments By Month");
    }
}
