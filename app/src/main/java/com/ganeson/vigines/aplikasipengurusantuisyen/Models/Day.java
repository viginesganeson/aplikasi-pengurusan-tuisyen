package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

import java.util.ArrayList;
import java.util.List;

public class Day {

    String id, day;

    public Day() { }

    public Day(String id, String day) {
        this.id = id;
        this.day = day;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

//    public List<String> getDayss(){
//        List<String> arrayList = new ArrayList();
//        arrayList.add("Monday");
//        arrayList.add("Tuesday");
//        arrayList.add("Wednesday");
//        arrayList.add("Thursday");
//        arrayList.add("Friday");
//        arrayList.add("Saturday");
//        arrayList.add("Sunday");
//        return arrayList;
//    }
}
