package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Attendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherEditAttendance;

import java.util.List;

public class TeacherViewDatesForAttendance extends RecyclerView.Adapter<TeacherViewDatesForAttendance.TeacherEditStudentsByClassForAttendanceViewHolder> {

    private Context context;
    private List<Attendance> attendanceList;
    private CoordinatorLayout coordinatorLayout;
    private String todayDate;

    public TeacherViewDatesForAttendance(Context context, List<Attendance> attendanceList, CoordinatorLayout coordinatorLayout, String todayDate) {
        this.context = context;
        this.attendanceList = attendanceList;
        this.coordinatorLayout = coordinatorLayout;
        this.todayDate = todayDate;
    }

    @NonNull
    @Override
    public TeacherEditStudentsByClassForAttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_teacher_view_dates_for_attendance,null);
        return new TeacherViewDatesForAttendance.TeacherEditStudentsByClassForAttendanceViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherEditStudentsByClassForAttendanceViewHolder teacherEditStudentsByClassForAttendanceViewHolder, final int i) {
        teacherEditStudentsByClassForAttendanceViewHolder.tvDate.setText(attendanceList.get(i).getDate());

        teacherEditStudentsByClassForAttendanceViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TeacherEditAttendance.class);
                intent.putExtra("CLASS_ID", attendanceList.get(i).getClassId());
                intent.putExtra("DATE", attendanceList.get(i).getDate());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return attendanceList.size();
    }

    public class TeacherEditStudentsByClassForAttendanceViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDate;
        private CardView cardView;

        public TeacherEditStudentsByClassForAttendanceViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tv_date);
            cardView = itemView.findViewById(R.id.rcview_teacher_view_date_for_attendance_cview);

        }
    }
}
