package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewStudentPayment;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

import java.util.Calendar;
import java.util.List;

public class AdminViewMonthsForStudentPaymentAdapter extends RecyclerView.Adapter<AdminViewMonthsForStudentPaymentAdapter.AdminViewMonthsForStudentPaymentViewHolder> {

    private List<Month> monthList;
    private Context context;
    private String studentId;

    public AdminViewMonthsForStudentPaymentAdapter(Context context, List<Month> monthList, String studentId){
        this.context = context;
        this.monthList = monthList;
        this.studentId = studentId;
    }

    @NonNull
    @Override
    public AdminViewMonthsForStudentPaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_months_for_payment,null);
        return new AdminViewMonthsForStudentPaymentAdapter.AdminViewMonthsForStudentPaymentViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminViewMonthsForStudentPaymentViewHolder adminViewMonthsForStudentPaymentViewHolder, int i) {
        final int year = Calendar.getInstance().get(Calendar.YEAR);
        adminViewMonthsForStudentPaymentViewHolder.tvMonth.setText(monthList.get(i).getMonth());
        adminViewMonthsForStudentPaymentViewHolder.tvYear.setText(Integer.toString(year));
        adminViewMonthsForStudentPaymentViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminViewStudentPayment.class);
                intent.putExtra("MONTH", monthList.get(adminViewMonthsForStudentPaymentViewHolder.getAdapterPosition()).getMonth());
                intent.putExtra("YEAR", Integer.toString(year));
                intent.putExtra("STUDENT_ID", studentId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return monthList.size();
    }

    class AdminViewMonthsForStudentPaymentViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMonth, tvYear;
        private CardView cardView;
        AdminViewMonthsForStudentPaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            cardView = itemView.findViewById(R.id.rcview_view_months_for_payment_cview);
        }
    }
}
