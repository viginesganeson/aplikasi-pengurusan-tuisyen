package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Month {

    private String id, month;

    public Month() {
    }

    public Month(String id, String month) {
        this.id = id;
        this.month = month;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
