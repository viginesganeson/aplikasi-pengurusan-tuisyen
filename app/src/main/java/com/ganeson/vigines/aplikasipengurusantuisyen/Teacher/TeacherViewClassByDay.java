package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentViewClassByDay;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class TeacherViewClassByDay extends AppCompatActivity {

    private String DAY;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Class> classList;
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private String LOCAL_USER_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constant.LOG_ACTIVITY, "TeacherViewClassByDay");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_view_class_by_day);
        loadLocalStorageData();

        Intent intent = getIntent();
        DAY = intent.getStringExtra("DAY");

        toolbar = findViewById(R.id.tlb_teacher_view_class_by_day);
        recyclerView = findViewById(R.id.rcview_teacher_view_class_by_day);
        linearLayoutManager = new LinearLayoutManager(TeacherViewClassByDay.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = findViewById(R.id.tv_empty_class);
        coordinatorLayout = findViewById(R.id.cl_teacher_view_class_by_day);

        setToolbar(toolbar, DAY);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar, String day){
        toolbar.setTitle(day);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadClass();
    }

    private void loadClass(){
        classList = new ArrayList<>();
        new ClassService().getClassByDayForTeacher(TeacherViewClassByDay.this, LOCAL_USER_ID, DAY, classList, textView, recyclerView, coordinatorLayout);
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(this);
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
