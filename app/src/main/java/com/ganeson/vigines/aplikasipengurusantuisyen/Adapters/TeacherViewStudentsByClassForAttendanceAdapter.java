package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Attendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TeacherViewStudentsByClassForAttendanceAdapter extends RecyclerView.Adapter<TeacherViewStudentsByClassForAttendanceAdapter.TeacherViewStudentsByClassForAttendanceViewHolder> {

    private Context context;
    private CoordinatorLayout coordinatorLayout;
    private List<Timetable> timetableList;
    private Timetable timetable;
    private User user;
    private List<User> userList;
    private String todayDate;
    private List<Attendance> attendanceList;
    private Attendance attendance;

    public TeacherViewStudentsByClassForAttendanceAdapter(Context context, List<Timetable> timetableList, CoordinatorLayout coordinatorLayout, String todayDate) {
        this.context = context;
        this.timetableList = timetableList;
        this.coordinatorLayout = coordinatorLayout;
        this.todayDate = todayDate;
    }

    @NonNull
    @Override
    public TeacherViewStudentsByClassForAttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_teacher_view_student_by_class_for_attendance,null);
        return new TeacherViewStudentsByClassForAttendanceAdapter.TeacherViewStudentsByClassForAttendanceViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TeacherViewStudentsByClassForAttendanceViewHolder teacherViewStudentsByClassForAttendanceViewHolder, final int i) {
        loadStudents(timetableList.get(i).getStudentID(), teacherViewStudentsByClassForAttendanceViewHolder);
        loadAttendance(timetableList.get(i).getStudentID(), timetableList.get(i).getClassID(), teacherViewStudentsByClassForAttendanceViewHolder);

        teacherViewStudentsByClassForAttendanceViewHolder.switchAttendace.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    teacherViewStudentsByClassForAttendanceViewHolder.switchAttendace.setText("Attend");
                    checkAttendanceAlreadyAdded(timetableList.get(i).getClassID(), timetableList.get(i).getStudentID(), "MODE_ADD");
                } else {
                    teacherViewStudentsByClassForAttendanceViewHolder.switchAttendace.setText("Absent");
                    checkAttendanceAlreadyAdded(timetableList.get(i).getClassID(), timetableList.get(i).getStudentID(), "MODE_DELETE");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return timetableList.size();
    }

    public class TeacherViewStudentsByClassForAttendanceViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private CardView cardView;
        private Switch switchAttendace;

        public TeacherViewStudentsByClassForAttendanceViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_student_name);
            switchAttendace = itemView.findViewById(R.id.swt_attendance);
            cardView = itemView.findViewById(R.id.rcview_teacher_view_student_by_class_for_attendance_cview);

        }
    }

    private void loadStudents(String userId, final TeacherViewStudentsByClassForAttendanceViewHolder teacherViewStudentsByClassForAttendanceViewHolder ) {
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
        databaseReference.orderByChild("id").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }
                if (dataSnapshot.exists()) {
                    for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()) {
                        user = userDataSnapshot.getValue(User.class);
                        userList.add(user);
                    }
                    teacherViewStudentsByClassForAttendanceViewHolder.tvName.setText(userList.get(0).getName());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    private void loadAttendance(String userId, final String classId, final TeacherViewStudentsByClassForAttendanceViewHolder teacherViewStudentsByClassForAttendanceViewHolder ) {
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_ATTENDANCE);
        databaseReference.orderByChild("studentId").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                attendanceList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }
                if (dataSnapshot.exists()) {
                    for (DataSnapshot attendanceDataSnapshot : dataSnapshot.getChildren()) {
                        attendance = attendanceDataSnapshot.getValue(Attendance.class);
                        if (attendance.getDate().equals(todayDate) && attendance.getClassId().equals(classId)){
                            attendanceList.add(attendance);
                        }
                    }

                    if (attendanceList.size() > 0) {
                        teacherViewStudentsByClassForAttendanceViewHolder.switchAttendace.setChecked(true);
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    private void addAttendance(String studentID, String classID) {
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_ATTENDANCE);
        String id = databaseReference.push().getKey();
        Attendance attendance = new Attendance(id, studentID, classID, todayDate);
        databaseReference.child(id).setValue(attendance)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
    }

    private void deleteAttendance(String attendanceId) {
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_ATTENDANCE);
        databaseReference.child(attendanceId).removeValue()
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
    }


    private void checkAttendanceAlreadyAdded(final String classId, final String studentId, final String MODE) {
        final List <Attendance> attendanceList = new ArrayList<>();
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_ATTENDANCE);
        databaseReference.orderByChild("classId").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {

                }

                if (dataSnapshot.exists()) {
                    for (DataSnapshot attendanceDataSnapshot : dataSnapshot.getChildren()) {
                        attendance = attendanceDataSnapshot.getValue(Attendance.class);
                        if (attendance.getDate().equals(todayDate) && attendance.getStudentId().equals(studentId)){
                            attendanceList.add(attendance);
                        }
                    }
                }

                Log.v("id", Integer.toString(attendanceList.size()));
                if (attendanceList.size() == 0) {
                    if (MODE.equals("MODE_ADD")){
                        addAttendance(studentId, classId);
                    }
                }

                if(attendanceList.size() >  0) {
                     if (MODE.equals("MODE_DELETE")) {
                        deleteAttendance(attendanceList.get(0).getId());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

}
