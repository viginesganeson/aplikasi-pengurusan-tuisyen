package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.StudentViewClassByDayAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewStudentsByClassForAttendanceAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.StudentViewClassNotesDownloadAdapter;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

public class TimetableService extends GlobalService {

    private DatabaseReference databaseReference;
    private Timetable timetable;

    public TimetableService () {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_TIME_TABLE);
    }

    public void getClassByDay(final Context context, final String userId, String day, final List<Timetable> timetableList, final TextView textView, final RecyclerView recyclerView, final CoordinatorLayout coordinatorLayout) {
        databaseReference.orderByChild("day").equalTo(day).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                }

                for (DataSnapshot timeTableDataSnapshot : dataSnapshot.getChildren()) {
                    timetable = timeTableDataSnapshot.getValue(Timetable.class);
                    if (timetable != null) {
                        if (timetable.getStudentID().equals(userId)){
                            timetableList.add(timetable);
                        }
                    }
                }

                recyclerView.setAdapter(new StudentViewClassByDayAdapter(context, timetableList, coordinatorLayout));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addClass(final Context context, String studentId, String classId, final String day, final CoordinatorLayout coordinatorLayout){
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Adding class...");
        progressDialog.show();
        String id = databaseReference.push().getKey();
        Timetable timetable = new Timetable(id, studentId, classId, day);
        if (id != null) {
            databaseReference.child(id).setValue(timetable)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Class Added");
                        progressDialog.dismiss();
                        new GlobalService().destroyAct(context);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showSnackBar(coordinatorLayout, e.getMessage());
                        progressDialog.dismiss();
                    }
                });
        }
    }

//    public void checkClassIsAdded(final Context context, final String studentId, final String classId, final String day, final CoordinatorLayout coordinatorLayout){
//        final List<Timetable> timetableList = new ArrayList<>();
//        databaseReference.orderByChild("studentID").equalTo(studentId).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                if (!dataSnapshot.exists()) {
//                    showSnackBar(coordinatorLayout, "No Class Registered For This Student");
//                }
//                if (dataSnapshot.exists()) {
//                    for (DataSnapshot timeTableDataSnapshot : dataSnapshot.getChildren()) {
//                        timetable = timeTableDataSnapshot.getValue(Timetable.class);
//                        if (timetable != null) {
//                            if (!timetable.getDay().equals(day) && !timetable.getClassID().equals(classId)){
//                                timetableList.add(timetable);
//                            } else {
//                                showSnackBar(coordinatorLayout, "Class Already Registered For This Student");
//                            }
//                        }
//                    }
//                    if (timetableList.size() > 0){
//                        addClass(context, studentId, classId, day, coordinatorLayout);
//
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }

    public void deleteTimeTable(String id, final Context context, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Deleting class...");
        progressDialog.show();
        databaseReference.child(id).removeValue()
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "Class Deleted");
                    progressDialog.dismiss();
                    new GlobalService().destroyAct(context);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                }
            });
    }

    public void getClassByStudent(final Context context, String studentID, final List<Timetable> timetableList, final TextView textView, final RecyclerView recyclerView){
        databaseReference.orderByChild("studentID").equalTo(studentID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot timeTableDataSnapshot : dataSnapshot.getChildren()) {
                        timetable = timeTableDataSnapshot.getValue(Timetable.class);
                        timetableList.add(timetable);
                    }
                }

                recyclerView.setAdapter(new StudentViewClassNotesDownloadAdapter(context, timetableList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getStudentByClass(final Context context, final String classID, final String date, final List<Timetable> timetableList, final TextView textView, final RecyclerView recyclerView, final CoordinatorLayout coordinatorLayout){
        databaseReference.orderByChild("classID").equalTo(classID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot timeTableDataSnapshot : dataSnapshot.getChildren()) {
                        timetable = timeTableDataSnapshot.getValue(Timetable.class);
                        timetableList.add(timetable);
                    }
                }
                recyclerView.setAdapter(new TeacherViewStudentsByClassForAttendanceAdapter(context, timetableList, coordinatorLayout, date));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    public void getClassDashboardForStudent(String studentID, final List<Timetable> timetableList, final TextView textView){
        databaseReference.orderByChild("studentID").equalTo(studentID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot timeTableDataSnapshot : dataSnapshot.getChildren()) {
                        timetable = timeTableDataSnapshot.getValue(Timetable.class);
                        timetableList.add(timetable);
                    }
                }
                textView.setText("0");
                if (timetableList.size() > 0) {
                    textView.setText(String.format(Locale.getDefault(), "%d", timetableList.size()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
