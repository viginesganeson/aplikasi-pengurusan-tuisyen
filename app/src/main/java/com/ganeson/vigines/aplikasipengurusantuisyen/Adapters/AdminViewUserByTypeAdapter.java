package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewUser;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

import java.util.ArrayList;
import java.util.List;

public class AdminViewUserByTypeAdapter extends RecyclerView.Adapter<AdminViewUserByTypeAdapter.AdminViewUserByTypeViewHolder> implements Filterable {

    private List<User> userList;
    private Context context;
    private List<User> fullUserList;

    public AdminViewUserByTypeAdapter(Context context, List<User> userList){
        this.userList = userList;
        this.context = context;
        this.fullUserList = new ArrayList<>(userList);
    }
    @NonNull
    @Override
    public AdminViewUserByTypeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_admin_view_user_by_type,null);
        AdminViewUserByTypeAdapter.AdminViewUserByTypeViewHolder crsv = new AdminViewUserByTypeAdapter.AdminViewUserByTypeViewHolder(layoutView);
        return crsv;
    }

    @Override
    public void onBindViewHolder(@NonNull AdminViewUserByTypeViewHolder adminViewUserByTypeViewHolder, final int i) {
        adminViewUserByTypeViewHolder.tvUserName.setText(userList.get(i).getName());
        adminViewUserByTypeViewHolder.tvUserPhone.setText(userList.get(i).getPhone());
        adminViewUserByTypeViewHolder.tvUserEmail.setText(userList.get(i).getEmail());

        if (userList.get(i).getActive()){
            adminViewUserByTypeViewHolder.tvuserActive.setVisibility(View.VISIBLE);
        } else {
            adminViewUserByTypeViewHolder.tvUserInactive.setVisibility(View.VISIBLE);
        }
        adminViewUserByTypeViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminViewUser.class);
                intent.putExtra("USER_ID", userList.get(i).getId());
                intent.putExtra("USER_STATUS", userList.get(i).getActive().toString());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class AdminViewUserByTypeViewHolder  extends RecyclerView.ViewHolder {
        private TextView tvUserName, tvUserEmail, tvUserPhone, tvuserActive, tvUserInactive;
        private CardView cardView;

        public AdminViewUserByTypeViewHolder(@NonNull View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tv_user_name);
            tvUserEmail = itemView.findViewById(R.id.tv_user_email);
            tvUserPhone = itemView.findViewById(R.id.tv_user_phone);
            cardView = itemView.findViewById(R.id.rcview_admin_view_user_by_type_cview);
            tvuserActive = itemView.findViewById(R.id.tv_user_active);
            tvUserInactive = itemView.findViewById(R.id.tv_user_inactive);
        }
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<User> filteredUserList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredUserList.addAll(fullUserList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (User user : fullUserList) {
                    if (user.getName().toLowerCase().contains(filterPattern)) {
                        filteredUserList.add(user);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredUserList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            userList.clear();
            userList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public Filter getFilter() {
        return filter;
    }
}
