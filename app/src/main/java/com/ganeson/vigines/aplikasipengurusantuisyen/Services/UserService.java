package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Auth.Login;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

public class UserService extends GlobalService {

    private User user;
    private DatabaseReference databaseReference;

    public UserService (){
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
    }

    void storeUser(User user, final CoordinatorLayout coordinatorLayout, final Context context) {

        databaseReference.child(user.getId()).setValue(user)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "Storing user details");
                    Intent intent = new Intent(context, Login.class);
                    context.startActivity(intent);
                    ((Activity)context).finish();
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showSnackBar(coordinatorLayout, e.getMessage());
                }
            });
    }

    void getUser(String email, final CoordinatorLayout coordinatorLayout) {

        databaseReference.orderByChild("email").equalTo(email).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    user = userDataSnapshot.getValue(User.class);
                    new GlobalService().writeLocalStorage(Constant.LOCAL_USER_TYPE, user.getUser_type());
                    new GlobalService().writeLocalStorage(Constant.LOCAL_USER_EMAIL, user.getEmail());
                    new GlobalService().writeLocalStorage(Constant.LOCAL_USER_ID, user.getId());
                    if (user.getImgUrl() != null){
                        new GlobalService().writeLocalStorage(Constant.LOCAL_USER_IMG_URL, user.getImgUrl());
                    } else {
                        new GlobalService().writeLocalStorage(Constant.LOCAL_USER_IMG_URL, "");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    public void updateUserStatus(final Context context, Boolean status, final String id, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Updating user...");
        progressDialog.show();
        databaseReference.child(id).child("active").setValue(status)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "User Updated");
                    new NotificationService().sendNotification(id, "Update", "User Updated");
                    progressDialog.dismiss();
                    new GlobalService().destroyAct(context);

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showSnackBar(coordinatorLayout, e.getMessage());
                    progressDialog.dismiss();
                }
            });
    }

    public void updateUser(String id, String name, String email, String phone, Integer userType, String icNo, String url, String signUpMonth, String gender, String dob, final CoordinatorLayout coordinatorLayout, final ProgressDialog progressDialog) {
        User user = new User(id, name, icNo, email, phone, dob,gender, userType, true, url, signUpMonth);
        databaseReference.child(id).setValue(user)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "User Updated");
                    progressDialog.dismiss();

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showSnackBar(coordinatorLayout, e.getMessage());
                    progressDialog.dismiss();
                }
            });
    }

    public void getUserDashboardForAdmin(final List<User> userList, final TextView textView, Integer userType) {
        databaseReference.orderByChild("user_type").equalTo(userType).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()) {
                        user = userDataSnapshot.getValue(User.class);
                        if (user != null) {
                            userList.add(user);
                        }
                    }
                }

                if (userList.size() > 0) {
                    textView.setText(String.format(Locale.getDefault(), "%d", userList.size()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
