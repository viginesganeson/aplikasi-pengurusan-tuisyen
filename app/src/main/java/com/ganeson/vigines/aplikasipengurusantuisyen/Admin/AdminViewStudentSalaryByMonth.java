package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.MonthService;

import java.util.ArrayList;
import java.util.List;

public class AdminViewStudentSalaryByMonth extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<Month> monthList;
    private Toolbar toolbar;
    private String STUDENT_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_student_salary_by_month);
        STUDENT_ID = getIntent().getStringExtra("USER_ID");
        toolbar = findViewById(R.id.tlb_student_view_salary_by_month);
        recyclerView = findViewById(R.id.rcview_student_view_salary_by_month);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        setToolbar(toolbar);
    }

    private void loadMonths(){
        monthList = new ArrayList<>();
        new MonthService().getAllMonths(getApplicationContext(), monthList, recyclerView, Constant.TYPE_ADMIN, "ADMIN_VIEW_STUDENT_PAYMENT", STUDENT_ID);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMonths();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("View Payments By Month");
    }
}
