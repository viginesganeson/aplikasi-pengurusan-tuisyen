package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherViewAttendanceByClass;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TeacherViewClassByDayAdapter extends RecyclerView.Adapter<TeacherViewClassByDayAdapter.TeacherViewClassByDayViewHolder> {

    private Context context;
    private List<Class> classList;
    private CoordinatorLayout coordinatorLayout;
    private List<Subject> subjectList;
    private Subject subject;

    public TeacherViewClassByDayAdapter(Context context, List<Class> classList, CoordinatorLayout coordinatorLayout) {
        Log.i(Constant.LOG_ADAPTER, "TeacherViewClassByDayAdapter");
        this.context = context;
        this.classList = classList;
        this.coordinatorLayout = coordinatorLayout;
    }

    @NonNull
    @Override
    public TeacherViewClassByDayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_teacher_view_class_by_day,null);
        return new TeacherViewClassByDayAdapter.TeacherViewClassByDayViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final TeacherViewClassByDayViewHolder teacherViewClassByDayViewHolder, final int i) {
        loadSubject(classList.get(i).getSubjectId(), teacherViewClassByDayViewHolder);
        teacherViewClassByDayViewHolder.tvTime.setText(classList.get(i).getTime());
        teacherViewClassByDayViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TeacherViewAttendanceByClass.class);
                intent.putExtra("CLASS_ID", classList.get(i).getId());
                intent.putExtra("SUBJECT_NAME", teacherViewClassByDayViewHolder.tvSubjectName.getText());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public class TeacherViewClassByDayViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSubjectName, tvTime;
        private CardView cardView;

        public TeacherViewClassByDayViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubjectName = itemView.findViewById(R.id.tv_subject_name);
            tvTime = itemView.findViewById(R.id.tv_class_time);
            cardView = itemView.findViewById(R.id.rcview_teacher_view_class_by_day_cview);
        }
    }

    private void loadSubject(String subjectId, final TeacherViewClassByDayViewHolder teacherViewClassByDayViewHolder){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.orderByChild("id").equalTo(subjectId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }

                if (dataSnapshot.exists()) {
                    for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()) {
                        subject = subjectDataSnapshot.getValue(Subject.class);
                        subjectList.add(subject);
                    }
                    teacherViewClassByDayViewHolder.tvSubjectName.setText(subjectList.get(0).getName());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }
}
