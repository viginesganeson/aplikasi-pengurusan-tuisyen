package com.ganeson.vigines.aplikasipengurusantuisyen;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.UserService;
import com.github.abdularis.civ.AvatarImageView;
import com.github.abdularis.civ.CircleImageView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.UUID;

import io.paperdb.Paper;

import static android.app.Activity.RESULT_OK;

public class UserSetting extends Fragment {

    private static final int PICK_IMAGE_REQUEST = 1;
    private boolean imagestatus = false;
    private Uri imageUri;
    private TextInputEditText etName, etPhone, etIc;
    private Button btnUpdate, btnChangePass;
    private String local_user_id, local_user_email, IMG_URL_ORIGIN, SIGNUP_MONTH_ORIGIN, GENDER_ORIGIN, DOB_ORIGIN;
    private Integer local_user_type;
    private User user;
    private CoordinatorLayout coordinatorLayout;
    private CircleImageView circleImageView;

    public UserSetting() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_setting, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadLocalStorageData();
        coordinatorLayout = view.findViewById(R.id.cl_user_setting);
        etName = view.findViewById(R.id.et_user_name);
        etIc = view.findViewById(R.id.et_user_ic);
        etPhone = view.findViewById(R.id.et_user_phone);
        btnUpdate = view.findViewById(R.id.btn_update_user);
        btnChangePass = view.findViewById(R.id.btn_change_password);
        btnUpdate.setOnClickListener(this.updateUser);
        btnChangePass.setOnClickListener(this.changePass);
        circleImageView = view.findViewById(R.id.cimv_user_image);
        circleImageView.setOnClickListener(this.chooseImage);
    }

    @Override
    public void onResume() {
        super.onResume();
        populateUser();
    }

    private OnClickListener chooseImage = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_REQUEST);
        }
    };

    private OnClickListener updateUser = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final ProgressDialog progressDialog = new GlobalService().showProgressDialog(UserSetting.this.getContext(), "Updating user...");
            progressDialog.show();
            final String name = etName.getText().toString();
            final String phone = etPhone.getText().toString();
            final String icno = etIc.getText().toString();

            if (imagestatus) {
                String imageName = UUID.randomUUID().toString();
                final StorageReference userProfileFolder = new FirebaseService().initStorage().child(FirebaseService.STR_USER_PROFILE + imageName);
                userProfileFolder.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        userProfileFolder.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                new UserService().updateUser(local_user_id, name, local_user_email, phone, local_user_type, icno, uri.toString(), SIGNUP_MONTH_ORIGIN, GENDER_ORIGIN, DOB_ORIGIN, coordinatorLayout, progressDialog);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                            }
                        });

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                    }
                });
            } else {
                new UserService().updateUser(local_user_id, name, local_user_email, phone, local_user_type, icno, IMG_URL_ORIGIN , SIGNUP_MONTH_ORIGIN, GENDER_ORIGIN, DOB_ORIGIN, coordinatorLayout, progressDialog);
            }


        }
    };

    private OnClickListener changePass = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(UserSetting.this.getContext(), ChangePassword.class );
            startActivity(intent);
        }
    };

    private void populateUser() {
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
        databaseReference.orderByChild("id").equalTo(local_user_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        user = userDataSnapshot.getValue(User.class);
                        if (user != null) {
                            etName.setText(user.getName());
                            etPhone.setText(user.getPhone());
                            etIc.setText(user.getIc_no());

                            GENDER_ORIGIN = user.getGender();
                            DOB_ORIGIN = user.getDate_of_birth();
                            IMG_URL_ORIGIN = user.getImgUrl();
                            SIGNUP_MONTH_ORIGIN = user.getSignUpMonth();
                            if (!imagestatus){
                                Picasso.get().load(user.getImgUrl()).into(circleImageView);
                            }
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(UserSetting.this.getContext());
        local_user_id = Paper.book().read(Constant.LOCAL_USER_ID);
        local_user_email = Paper.book().read(Constant.LOCAL_USER_EMAIL);
        local_user_type = Paper.book().read(Constant.LOCAL_USER_TYPE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode== PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageUri = data.getData();
            if(imageUri != null) {
                imagestatus = true;
                showImage(imageUri);
            }
        }
    }

    private void showImage(@NonNull Uri imageUri) {
        try {
            circleImageView.setTag(imageUri);
            Bitmap bitmap = null;
            bitmap = MediaStore.Images.Media.getBitmap(UserSetting.this.getContext().getContentResolver(), imageUri);
            circleImageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
