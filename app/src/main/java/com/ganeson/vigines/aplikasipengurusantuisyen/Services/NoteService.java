package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.StudentViewNotesToDownloadAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewNotesByClassAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Note;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class NoteService extends GlobalService {

    private DatabaseReference databaseReference;
    private Note note;

    public NoteService (){
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_NOTE);
    }

    public void addNote(final CoordinatorLayout coordinatorLayout, String classId, String noteUrl){
        String id = databaseReference.push().getKey();
        Note note = new Note(id, classId, noteUrl);
        if (id != null) {
            databaseReference.child(id).setValue(note)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Notes Added");

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showSnackBar(coordinatorLayout, e.getMessage());
                    }
                });
        }
    }

    public void getNotesByClass(final Context context, String classId, final List<Note> noteList, final RecyclerView recyclerView, final TextView textView, final CoordinatorLayout coordinatorLayout) {
        noteList.clear();
        databaseReference.orderByChild("classId").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                        note = noteDataSnapshot.getValue(Note.class);
                        noteList.add(note);
                    }
                }

                recyclerView.setAdapter(new TeacherViewNotesByClassAdapter(context, noteList, coordinatorLayout));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getNotesByClassToDownload(final Context context, String classId, final List<Note> noteList, final RecyclerView recyclerView, final TextView textView, final CoordinatorLayout coordinatorLayout) {
        noteList.clear();
        databaseReference.orderByChild("classId").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                        note = noteDataSnapshot.getValue(Note.class);
                        noteList.add(note);
                    }
                }

                recyclerView.setAdapter(new StudentViewNotesToDownloadAdapter(context, noteList, coordinatorLayout));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void deleteNote(String id, final Context context, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Deleting notes...");
        progressDialog.show();
        databaseReference.child(id).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Notes Deleted");
                        progressDialog.dismiss();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                    }
                });
    }
}
