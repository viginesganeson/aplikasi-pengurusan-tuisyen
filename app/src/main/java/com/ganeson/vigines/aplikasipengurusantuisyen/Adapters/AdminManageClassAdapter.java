package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewClassByDay;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class AdminManageClassAdapter extends RecyclerView.Adapter<AdminManageClassAdapter.AdminManageClassViewHolder> {

    private List<Day> dayList;
    private Context context;

    public AdminManageClassAdapter(Context context, List<Day> dayList){
        this.dayList = dayList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdminManageClassAdapter.AdminManageClassViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_admin_manage_class,null);
        return new AdminManageClassAdapter.AdminManageClassViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminManageClassViewHolder adminManageClassViewHolder, int i) {

        adminManageClassViewHolder.tvDay.setText(dayList.get(i).getDay());
        countClass(dayList.get(i).getDay(), adminManageClassViewHolder.tvTotalClass);

        adminManageClassViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminViewClassByDay.class);
                intent.putExtra("DAY",dayList.get(adminManageClassViewHolder.getAdapterPosition()).getDay());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.dayList.size();
    }

    class AdminManageClassViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDay, tvTotalClass;
        private CardView cardView;

        AdminManageClassViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDay = itemView.findViewById(R.id.tv_day);
            tvTotalClass = itemView.findViewById(R.id.tv_total_class);
            cardView = itemView.findViewById(R.id.rcview_admin_manage_class_cview);
        }
    }

    private void countClass(String day, final TextView textView){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_CLASS);
        databaseReference.orderByChild("day").equalTo(day).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                textView.setText(Long.toString(dataSnapshot.getChildrenCount()));
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERROR", databaseError.getMessage());
            }
        });
    }
}
