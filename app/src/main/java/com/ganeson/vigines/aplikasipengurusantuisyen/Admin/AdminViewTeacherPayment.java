package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FeeService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SalaryService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentAddPayment;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AdminViewTeacherPayment extends AppCompatActivity {

    private String TEACHER_ID, MONTH, YEAR;
    private TextView textView;
    private RecyclerView recyclerView;
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private boolean FILE_STATUS = false;
    private Uri FILE_URI;
    private List<Salary> salaryList;
    private LinearLayoutManager linearLayoutManager;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_teacher_payment);
        TEACHER_ID = getIntent().getStringExtra("TEACHER_ID");
        MONTH = getIntent().getStringExtra("MONTH");
        YEAR = getIntent().getStringExtra("YEAR");

        coordinatorLayout = findViewById(R.id.cl_admin_view_teacher_payment);
        textView = findViewById(R.id.tv_empty_payments);
        toolbar = findViewById(R.id.tlb_admin_view_teacher_payment);
        recyclerView = findViewById(R.id.rcview_admin_view_teacher_payment);
        linearLayoutManager = new LinearLayoutManager(AdminViewTeacherPayment.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        floatingActionButton = findViewById(R.id.fab_admin_add_teacher_payment);

        setToolbar(toolbar);
        floatingActionButton.setOnClickListener(this.addSalary);
    }

    private View.OnClickListener addSalary = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setType("application/pdf");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"Select PDF"), 1);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        loadSalary();
    }

    private void loadSalary(){
        salaryList = new ArrayList<>();
        new SalaryService().getSalary(AdminViewTeacherPayment.this, salaryList, textView, recyclerView, TEACHER_ID, MONTH, YEAR, coordinatorLayout, "ADMIN_VIEW_TEACHER_PAYMENT");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(MONTH + " Payment" + " (" + YEAR +")");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            FILE_URI = data.getData();
            if(FILE_URI != null) {
                FILE_STATUS = true;
            }

            if (FILE_STATUS){
                String fileName = UUID.randomUUID().toString();
                final StorageReference salaryFolder = new FirebaseService().initStorage().child(FirebaseService.STR_SALARY + fileName);
                salaryFolder.putFile(FILE_URI)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                salaryFolder.getDownloadUrl()
                                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                new SalaryService().addSalary(coordinatorLayout, TEACHER_ID, MONTH, YEAR, uri.toString());
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                                            }
                                        });
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                            }
                        });
            } else {
                new GlobalService().showSnackBar(coordinatorLayout, "No file selected");
            }
        }
    }
}
