package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewClassByDay;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentViewClassByDay;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.paperdb.Paper;

public class StudentTimeTableAdapter extends RecyclerView.Adapter<StudentTimeTableAdapter.StudentTimeTableViewHolder> {

    private List<Day> dayList;
    private Context context;
    private Timetable timetable;
    private List<Timetable> timetableList;

    public StudentTimeTableAdapter(Context context, List<Day> dayList){
        this.context = context;
        this.dayList = dayList;
    }

    @NonNull
    @Override
    public StudentTimeTableViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_student_time_table,null);
        StudentTimeTableAdapter.StudentTimeTableViewHolder crsv = new StudentTimeTableAdapter.StudentTimeTableViewHolder(layoutView);
        return crsv;
    }

    @Override
    public void onBindViewHolder(@NonNull StudentTimeTableViewHolder studentTimeTableViewHolder, final int i) {
        studentTimeTableViewHolder.tvDay.setText(dayList.get(i).getDay());
        countClass(dayList.get(i).getDay(), studentTimeTableViewHolder.tvTotalClass);
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        // full name form of the day
        String today = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

        if (dayList.get(i).getDay().equals(today)){
            studentTimeTableViewHolder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorCardDashboard1));
        }

        studentTimeTableViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StudentViewClassByDay.class);
                intent.putExtra("DAY",dayList.get(i).getDay());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dayList.size();
    }

    public class StudentTimeTableViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDay, tvTotalClass;
        private CardView cardView;
        public StudentTimeTableViewHolder(@NonNull View itemView) {
            super(itemView);
            tvDay = itemView.findViewById(R.id.tv_day);
            tvTotalClass = itemView.findViewById(R.id.tv_total_class);
            cardView = itemView.findViewById(R.id.cview_student_time_table);
        }
    }

    private void countClass(String day, final TextView textView){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_TIME_TABLE);
        final String LOCAL_USER_ID = loadLocalStorageData();

        databaseReference.orderByChild("day").equalTo(day).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                timetableList = new ArrayList<>();
                for (DataSnapshot timeTableDataSnapshot : dataSnapshot.getChildren()) {
                    timetable = timeTableDataSnapshot.getValue(Timetable.class);
                    if (timetable.getStudentID().equals(LOCAL_USER_ID)){
                        timetableList.add(timetable);
                    }
                }
                textView.setText(Integer.toString(timetableList.size()));
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("ERROR", databaseError.getMessage());
            }
        });
    }

    // Load local storage data
    private String loadLocalStorageData (){
        Paper.init(this.context);
        return Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
