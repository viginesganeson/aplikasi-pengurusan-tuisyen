package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewClass;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class AdminViewClassByDayAdapter extends RecyclerView.Adapter<AdminViewClassByDayAdapter.AdminViewClassByDayViewHolder> {

    private List<Class> classList;
    private Context context;
    private User user;
    private Subject subject;

    public AdminViewClassByDayAdapter(Context context, List<Class> classList){
        this.classList = classList;
        this.context = context;
    }
    @NonNull
    @Override
    public AdminViewClassByDayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_admin_view_class_by_day,null);
        return new AdminViewClassByDayAdapter.AdminViewClassByDayViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminViewClassByDayViewHolder adminViewClassByDayViewHolder, int i) {
        adminViewClassByDayViewHolder.tvTime.setText(classList.get(i).getTime());
        getTeacherName(classList.get(i).getTeacherId(), adminViewClassByDayViewHolder.tvTeacherName);
        getSubjectName(classList.get(i).getSubjectId(), adminViewClassByDayViewHolder.tvSubjectName);

        adminViewClassByDayViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminViewClass.class);
                intent.putExtra("CLASS_ID", classList.get(adminViewClassByDayViewHolder.getAdapterPosition()).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.classList.size();
    }

    class AdminViewClassByDayViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTeacherName, tvSubjectName, tvTime;
        private CardView cardView;

        AdminViewClassByDayViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTeacherName = itemView.findViewById(R.id.tv_teacher_name);
            tvSubjectName = itemView.findViewById(R.id.tv_subject_name);
            tvTime = itemView.findViewById(R.id.tv_class_time);
            cardView = itemView.findViewById(R.id.rcview_admin_view_class_by_day_cview);
        }
    }

    private void getTeacherName(String teacherID, final TextView textView){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
        databaseReference.orderByChild("id").equalTo(teacherID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        user = userDataSnapshot.getValue(User.class);
                        if (user != null) {
                            textView.setText(user.getName());
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getSubjectName(String subjectId, final TextView textView){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.orderByChild("id").equalTo(subjectId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()){
                    if (subjectDataSnapshot.exists()){
                        subject = subjectDataSnapshot.getValue(Subject.class);
                        if (subject != null) {
                            textView.setText(subject.getName());
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
