package com.ganeson.vigines.aplikasipengurusantuisyen.Auth;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ganeson.vigines.aplikasipengurusantuisyen.Home;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.AuthService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import io.paperdb.Paper;

public class Login extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference dbrUser;
    private TextInputEditText txtEmail, txtPassword;
    private Button btnLogin;
    private TextView txtvSignUp,txtvForgotPassword;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.initFirebase();
        Paper.init(this);

        txtEmail = findViewById(R.id.txt_email);
        txtPassword = findViewById(R.id.txt_password);
        btnLogin = findViewById(R.id.btn_login);
        txtvForgotPassword = findViewById(R.id.txtv_forgot_password);
        txtvSignUp = findViewById(R.id.txtv_signup);
        coordinatorLayout = findViewById(R.id.cdlayout_snackbar);

        btnLogin.setOnClickListener(this.login);
        txtvForgotPassword.setOnClickListener(this.forgotPassword);
        txtvSignUp.setOnClickListener(this.signUp);

    }

    private void initFirebase() {
        firebaseAuth = new FirebaseService().initFirebase();
        dbrUser = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
    }

    //btn login click
    private View.OnClickListener login = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String email;
            String password;

            if (TextUtils.isEmpty(txtEmail.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout,"Email Required");
                txtEmail.requestFocus();
            } else if (TextUtils.isEmpty(txtPassword.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout, "Password Required");
                txtPassword.requestFocus();
            } else {
                email = txtEmail.getText().toString();
                password = txtPassword.getText().toString();
                new AuthService().loginUser(Login.this, coordinatorLayout, email, password);
            }

            //startActivity(new Intent(Login.this, Home.class));
        }
    };

    private View.OnClickListener forgotPassword = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            v = getLayoutInflater().inflate( R.layout.dialog_forgot_password, null );
            final EditText etEmail =  v.findViewById(R.id.et_email_forgot_password);
            final AlertDialog.Builder dialog = new AlertDialog.Builder(Login.this);
            dialog.setPositiveButton("Reset", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialogInterface, int i) {

                    if (TextUtils.isEmpty(etEmail.getText())) {
                        Toast.makeText(getApplicationContext(),"Empty Field",Toast.LENGTH_LONG).show();
                    } else {
                        FirebaseAuth.getInstance().sendPasswordResetEmail(etEmail.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(),"Email reset link have been sent to "+ etEmail.getText().toString(),Toast.LENGTH_LONG).show();
                                        dialogInterface.dismiss();
                                    }
                                }
                            });
                    }

                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.setTitle("Reset Password");
            dialog.setView(v);
            dialog.show();
        }
    };

    private View.OnClickListener signUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(getApplicationContext(),SignUpStaging.class));
        }
    };

}
