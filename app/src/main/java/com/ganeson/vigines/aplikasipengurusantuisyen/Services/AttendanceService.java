package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewDatesForAttendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Attendance;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class AttendanceService extends GlobalService {

    private DatabaseReference databaseReference;
    private Attendance attendance;

    public AttendanceService() {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_ATTENDANCE);
    }

    public void getAttendance(final Context context, final List<Attendance> attendanceList, String classId, final String todayDate, final TextView textView, final RecyclerView recyclerView, final CoordinatorLayout coordinatorLayout) {
        databaseReference.orderByChild("classId").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                attendanceList.clear();
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot attendanceDataSnapshot : dataSnapshot.getChildren()) {
                        attendance = attendanceDataSnapshot.getValue(Attendance.class);
                        attendanceList.add(attendance);
                    }
                }

//                List<Attendance> newAttendanceList = new ArrayList<>();
//
//                for (int i = 0; i < attendanceList.size(); i++) {
//                    newAttendanceList.add(attendanceList.get(i));
//                    if (i == 1) {
//                        int j;
//                        j = i-1;
//                        if (!newAttendanceList.get(j).getDate().equals(attendanceList.get(i).getDate())){
//                            newAttendanceList.add(attendanceList.get(i));
//                        }
//                    }
//                }

                recyclerView.setAdapter(new TeacherViewDatesForAttendance(context, attendanceList, coordinatorLayout, todayDate));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }
}
