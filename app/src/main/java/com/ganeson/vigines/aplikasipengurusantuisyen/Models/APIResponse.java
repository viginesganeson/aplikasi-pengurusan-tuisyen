package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

import java.util.List;

public class APIResponse {
    public long multicast_id;
    public int success;
    public int failure;
    public int canonical_ids;
    public List<Result> results;
}
