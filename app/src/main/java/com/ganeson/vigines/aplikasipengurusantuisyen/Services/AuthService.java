package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;

import com.ganeson.vigines.aplikasipengurusantuisyen.Home;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AuthService extends GlobalService {

    private FirebaseAuth firebaseAuth;

    public  AuthService(){
        firebaseAuth = new FirebaseService().initFirebase();
    }

    public void signUpUser(final Context context, final CoordinatorLayout coordinatorLayout, final String email, final String password, final String name, final String icno, final String phone, final String dateOfBirth, final String gender, final Integer userType, final String signUpMonth){

        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Registering user");
        progressDialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    progressDialog.dismiss();
                    showSnackBar(coordinatorLayout,"Register Success");
                    String id = firebaseAuth.getCurrentUser().getUid();
                    User user = new User(id, name, icno, email, phone, dateOfBirth, gender, userType, true, "", signUpMonth);
                    new UserService().storeUser(user, coordinatorLayout, context);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    showSnackBar(coordinatorLayout, e.getMessage());
                }
            });
    }

    public void loginUser(final Context context, final CoordinatorLayout coordinatorLayout, final String email, String password){

        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Signing in...");
        progressDialog.show();
        new UserService().getUser(email, coordinatorLayout);

        firebaseAuth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                @Override
                public void onSuccess(AuthResult authResult) {
                    progressDialog.dismiss();
                    Intent intent = new Intent(context, Home.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    context.startActivity(intent);
                    destroyActivity(context);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    showSnackBar(coordinatorLayout, e.getMessage());
                }
            });
    }

}
