package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Constant {

    // USER TYPE CONSTANTS
    public static Integer TYPE_STUDENT = 1;
    public static Integer TYPE_TEACHER = 2;
    public static Integer TYPE_PARENT = 3;
    public static Integer TYPE_ADMIN = 4;
    public static Integer TYPE_SUPER_ADMIN = 5;

    // LOCAL STORAGE CONSTANTS
    public static String LOCAL_USER_EMAIL = "LOCAL_USER_EMAIL";
    public static String LOCAL_USER_TYPE = "LOCAL_USER_TYPE";
    public static String LOCAL_USER_ID = "LOCAL_USER_ID";
    public static String LOCAL_USER_IMG_URL = "LOCAL_USER_IMG_URL";

    // LOG CONSTANTS
    public static String LOG_ADAPTER = "APP_ADAPTER";
    public static String LOG_ACTIVITY = "APP_ACTIVITY";
    public static String LOG_FRAGMENT = "APP_FRAGMENT";

}
