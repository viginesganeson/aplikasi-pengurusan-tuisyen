package com.ganeson.vigines.aplikasipengurusantuisyen.Student;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SalaryService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherDashboard;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.paperdb.Paper;

public class StudentDashboard extends Fragment {

    private TextView tvDate, tvTime, tvClassCount;
    private String LOCAL_USER_ID;
    private List<Timetable> timetableList;

    public StudentDashboard() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_student_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadLocalStorageData();
        tvClassCount = view.findViewById(R.id.tv_student_class_count);
        tvTime = view.findViewById(R.id.tv_time);
        tvDate = view.findViewById(R.id.tv_date);
        setDateTime();

        Thread myThread = null;
        Runnable runnable = new CountDownRunner();
        myThread = new Thread(runnable);
        myThread.start();

    }

    private void setDateTime() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String day = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        String month = Calendar.getInstance().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        String dateToday = Integer.toString(Calendar.getInstance().get(Calendar.DATE));
        tvDate.setText(day + " " + dateToday + "-" + month + "-" + year);
    }

    class CountDownRunner implements Runnable{
        public void run() {
            while(!Thread.currentThread().isInterrupted()){
                try {
                    doWork();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    public void doWork() {
        if (StudentDashboard.this.getActivity() != null) {
            StudentDashboard.this.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Date date = new Date();
                    int hours = date.getHours();
                    int minutes = date.getMinutes();
                    int seconds = date.getSeconds();
                    String currrentTime = String.format(Locale.getDefault(),"%02d:%02d:%02d", hours, minutes, seconds);
                    tvTime.setText(currrentTime);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.loadDashboard();
    }

    private void loadDashboard() {
        timetableList = new ArrayList<>();
        new TimetableService().getClassDashboardForStudent(LOCAL_USER_ID, timetableList, tvClassCount);
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(StudentDashboard.this.getContext());
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
