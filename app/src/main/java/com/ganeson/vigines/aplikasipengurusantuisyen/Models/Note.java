package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Note {

    public String id, classId, noteUrl;

    public Note () {}

    public Note(String id, String classId, String noteUrl) {
        this.id = id;
        this.classId = classId;
        this.noteUrl = noteUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getNoteUrl() {
        return noteUrl;
    }

    public void setNoteUrl(String noteUrl) {
        this.noteUrl = noteUrl;
    }
}
