package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SubjectService;

public class AdminAddSubject extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btnAddSubject;
    private CoordinatorLayout coordinatorLayout;
    private TextInputEditText txtName, txtDesc, txtCode;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_subject);

        toolbar = findViewById(R.id.tlb_add_subject);
        btnAddSubject = findViewById(R.id.btn_add_subject);
        coordinatorLayout = findViewById(R.id.cdlayout_admin_add_subject);
        relativeLayout = findViewById(R.id.rlayout_admin_add_subject);
        txtName = findViewById(R.id.txt_subject_name);
        txtDesc = findViewById(R.id.txt_subject_desc);
        txtCode = findViewById(R.id.txt_subject_code);
        setToolbar(toolbar);
        btnAddSubject.setOnClickListener(this.addSubject);
    }


    private View.OnClickListener addSubject = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);

            String name;
            String desc;
            String code;

            if (TextUtils.isEmpty(txtName.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout,"Name Required");
                txtName.requestFocus();
            } else if (TextUtils.isEmpty(txtCode.getText())){
                new GlobalService().showSnackBar(coordinatorLayout,"Code Required");
                txtCode.requestFocus();
            } else {
                name = txtName.getText().toString();
                desc = txtDesc.getText().toString();
                code = txtCode.getText().toString();
                new SubjectService().addSubject(AdminAddSubject.this, coordinatorLayout, name, code, desc);
            }
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
