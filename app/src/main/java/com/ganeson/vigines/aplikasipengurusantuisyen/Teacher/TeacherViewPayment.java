package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;

import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Fee;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FeeService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SalaryService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentAddPayment;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class TeacherViewPayment extends AppCompatActivity {

    private TextView textView;
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<Salary> salaryList;
    private Toolbar toolbar;
    private String YEAR, MONTH, LOCAL_USER_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_view_payment);
        loadLocalStorageData();

        MONTH = getIntent().getStringExtra("MONTH");
        YEAR = getIntent().getStringExtra("YEAR");

        coordinatorLayout = findViewById(R.id.cl_teacher_view_payment);
        textView = findViewById(R.id.tv_empty_payments);
        toolbar = findViewById(R.id.tlb_teacher_view_payment);
        recyclerView = findViewById(R.id.rcview_teacher_payments);
        linearLayoutManager = new LinearLayoutManager(TeacherViewPayment.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        setToolbar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadSalary();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(MONTH + " Payment" + " (" + YEAR +")");
    }

    private void loadSalary(){
        salaryList = new ArrayList<>();
        new SalaryService().getSalary(TeacherViewPayment.this, salaryList, textView, recyclerView, LOCAL_USER_ID, MONTH, YEAR, coordinatorLayout, "TEACHER_VIEW_PAYMENT");

    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(this);
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
