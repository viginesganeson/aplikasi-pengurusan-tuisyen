package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.NoteService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SalaryService;

import java.util.List;

public class AdminViewTeacherPaymentListAdapter extends RecyclerView.Adapter<AdminViewTeacherPaymentListAdapter.AdminViewTeacherPaymentListViewHolder> {

    private List<Salary> salaryList;
    private Context context;
    private CoordinatorLayout coordinatorLayout;

    public AdminViewTeacherPaymentListAdapter(Context context, List<Salary> salaryList, CoordinatorLayout coordinatorLayout){
        this.context = context;
        this.salaryList = salaryList;
        this.coordinatorLayout = coordinatorLayout;
    }
    @NonNull
    @Override
    public AdminViewTeacherPaymentListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_payment,null);
        return new AdminViewTeacherPaymentListAdapter.AdminViewTeacherPaymentListViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdminViewTeacherPaymentListViewHolder adminViewTeacherPaymentListViewHolder, final int i) {
        adminViewTeacherPaymentListViewHolder.tvYear.setText(salaryList.get(i).getYear());
        adminViewTeacherPaymentListViewHolder.tvFeeId.setText(salaryList.get(i).getId());
        adminViewTeacherPaymentListViewHolder.tvMonth.setText(salaryList.get(i).getMonth());

        adminViewTeacherPaymentListViewHolder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder
                    .setMessage("Are you sure to delete this file ?")
                    .setTitle("Delete File")
                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new SalaryService().deleteSalary(salaryList.get(i).getId(), context, coordinatorLayout);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return salaryList.size();
    }

    public class AdminViewTeacherPaymentListViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMonth, tvYear, tvFeeId, tvStatusUnapproved, tvStatusApproved;
        private CardView cardView;

        public AdminViewTeacherPaymentListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            tvFeeId = itemView.findViewById(R.id.tv_payment_id);
            cardView = itemView.findViewById(R.id.rcview_view_payment_cview);
            tvStatusUnapproved = itemView.findViewById(R.id.tv_status_unapproved);
            tvStatusApproved = itemView.findViewById(R.id.tv_status_approved);
        }
    }
}
