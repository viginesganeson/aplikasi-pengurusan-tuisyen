package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewDatesForAttendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Attendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.AttendanceService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TeacherAddAttendance extends AppCompatActivity {

    private Toolbar toolbar;
    private String CLASS_ID, TODAY_DATE;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    List<Timetable> timetableList;
    private CoordinatorLayout coordinatorLayout;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_add_attendance);
        loadTodayDate();

        CLASS_ID = getIntent().getStringExtra("CLASS_ID");

        toolbar = findViewById(R.id.tlb_teacher_add_attendance);
        recyclerView = findViewById(R.id.rcview_teacher_add_attendance);
        linearLayoutManager = new LinearLayoutManager(TeacherAddAttendance.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        coordinatorLayout = findViewById(R.id.cl_teacher_add_attendance);
        textView = findViewById(R.id.tv_empty_students);
        setToolbar(toolbar);

    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        toolbar.setTitle("Add Attendance - " + TODAY_DATE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStudentsByClass();
    }

    private void loadStudentsByClass(){
        timetableList = new ArrayList<>();
        new TimetableService().getStudentByClass(TeacherAddAttendance.this, CLASS_ID, TODAY_DATE, timetableList, textView, recyclerView, coordinatorLayout);
    }

    private void loadTodayDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        TODAY_DATE = formatter.format(calendar.getTime());
    }

}
