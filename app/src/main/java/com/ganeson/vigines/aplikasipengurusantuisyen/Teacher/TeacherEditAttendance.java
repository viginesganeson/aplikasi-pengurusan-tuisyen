package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;

import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TeacherEditAttendance extends AppCompatActivity {

    private Toolbar toolbar;
    private String CLASS_ID, DATE;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    List<Timetable> timetableList;
    private CoordinatorLayout coordinatorLayout;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_edit_attendance);

        CLASS_ID = getIntent().getStringExtra("CLASS_ID");
        DATE = getIntent().getStringExtra("DATE");

        toolbar = findViewById(R.id.tlb_teacher_edit_attendance);
        recyclerView = findViewById(R.id.rcview_teacher_edit_attendance);
        linearLayoutManager = new LinearLayoutManager(TeacherEditAttendance.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        coordinatorLayout = findViewById(R.id.cl_teacher_edit_attendance);
        textView = findViewById(R.id.tv_empty_students);
        setToolbar(toolbar);
    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        toolbar.setTitle("View Attendance - " + DATE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStudentsByClass();
    }

    private void loadStudentsByClass(){
        timetableList = new ArrayList<>();
        new TimetableService().getStudentByClass(TeacherEditAttendance.this, CLASS_ID, DATE, timetableList, textView, recyclerView, coordinatorLayout);
    }

}
