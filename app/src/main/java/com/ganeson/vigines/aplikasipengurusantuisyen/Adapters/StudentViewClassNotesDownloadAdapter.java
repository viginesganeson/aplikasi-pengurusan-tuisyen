package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentViewNotesByClass;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class StudentViewClassNotesDownloadAdapter extends RecyclerView.Adapter<StudentViewClassNotesDownloadAdapter.StudentViewClassNotesDownloadViewHolder> {

    private List<Timetable> timetableList;
    private Context context;
    private Class aClass;
    private Subject subject;
    private List<Class> classList;
    private List<Subject> subjectList;

    public StudentViewClassNotesDownloadAdapter(Context context, List<Timetable> timetableList){
        this.context = context;
        this.timetableList = timetableList;
    }

    @NonNull
    @Override
    public StudentViewClassNotesDownloadViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_student_view_class_download_notes,null);
        StudentViewClassNotesDownloadAdapter.StudentViewClassNotesDownloadViewHolder crsv = new StudentViewClassNotesDownloadAdapter.StudentViewClassNotesDownloadViewHolder(layoutView);
        return crsv;
    }

    @Override
    public void onBindViewHolder(@NonNull final StudentViewClassNotesDownloadViewHolder studentViewClassNotesDownloadViewHolder, final int i) {
        studentViewClassNotesDownloadViewHolder.tvDay.setText(timetableList.get(i).getDay());
        loadClass(timetableList.get(i).getClassID(),studentViewClassNotesDownloadViewHolder);

        studentViewClassNotesDownloadViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String SUBJECT_NAME = studentViewClassNotesDownloadViewHolder.tvSubject.getText().toString();
                Intent intent = new Intent(context, StudentViewNotesByClass.class);
                intent.putExtra("CLASS_ID", timetableList.get(i).getClassID());
                intent.putExtra("SUBJECT_NAME", SUBJECT_NAME);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return timetableList.size();
    }

    public class StudentViewClassNotesDownloadViewHolder extends RecyclerView.ViewHolder {
        TextView tvSubject, tvDay;
        CardView cardView;
        public StudentViewClassNotesDownloadViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubject = itemView.findViewById(R.id.tv_subject_name);
            tvDay = itemView.findViewById(R.id.tv_day);
            cardView = itemView.findViewById(R.id.rcview_student_view_class_download_notes_cview);
        }
    }

    private void loadClass(final String classId, final StudentViewClassNotesDownloadAdapter.StudentViewClassNotesDownloadViewHolder studentViewClassNotesDownloadViewHolder){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_CLASS);
        databaseReference.orderByChild("id").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                classList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }
                if (dataSnapshot.exists()) {
                    for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()) {
                        aClass = classDataSnapshot.getValue(Class.class);
                        classList.add(aClass);
                    }
                    loadSubject(classList.get(0).getSubjectId(), studentViewClassNotesDownloadViewHolder);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadSubject(String subjectId, final StudentViewClassNotesDownloadAdapter.StudentViewClassNotesDownloadViewHolder studentViewClassNotesDownloadViewHolder){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.orderByChild("id").equalTo(subjectId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }

                if (dataSnapshot.exists()) {
                    for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()) {
                        subject = subjectDataSnapshot.getValue(Subject.class);
                        subjectList.add(subject);
                    }
                    studentViewClassNotesDownloadViewHolder.tvSubject.setText(subjectList.get(0).getName());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
