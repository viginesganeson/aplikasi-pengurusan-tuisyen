package com.ganeson.vigines.aplikasipengurusantuisyen;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminDashboard;
import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminManageClass;
import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminManagePayment;
import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminManageSubject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminManageUserProfile;
import com.ganeson.vigines.aplikasipengurusantuisyen.Auth.Login;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Token;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.DayService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.MonthService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimeService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentDashboard;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentDownload;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentPayment;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentTimeTable;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherDashboard;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherPayment;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherStudentAttendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherUpload;
import com.github.abdularis.civ.CircleImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import io.paperdb.Paper;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Integer local_user_type;
    private String local_user_email, local_user_img_url;
    private Toolbar toolbar;
    private TextView tvUserEmail, tvUserRole;
    private CircleImageView circleImageView;

    @Override
    protected void onStart() {
        super.onStart();
        new DayService().checkIfExist();
        new TimeService().checkIfExist();
        new MonthService().checkIfExist();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        loadLocalStorageData();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        setUserMenu(navigationView);
        View topView = navigationView.getHeaderView(0);
        tvUserEmail = topView.findViewById(R.id.tv_login_email);
        tvUserRole = topView.findViewById(R.id.tv_login_role);
        circleImageView = topView.findViewById(R.id.cimv_user_image);
        setUserDetails();
        updateToken(FirebaseInstanceId.getInstance().getToken());

    }

    private void setUserDetails() {
        tvUserEmail.setText(local_user_email);
        if (!local_user_img_url.equals("")){
            Picasso.get().load(local_user_img_url).into(circleImageView);
        }
        if (local_user_type.equals(Constant.TYPE_ADMIN)){
            tvUserRole.setText(getResources().getString(R.string.admin_uppercase));
        } else if (local_user_type.equals(Constant.TYPE_TEACHER)){
            tvUserRole.setText(getResources().getString(R.string.teacher_uppercase));
        } else if (local_user_type.equals(Constant.TYPE_STUDENT)) {
            tvUserRole.setText(getResources().getString(R.string.student_uppercase));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        int id = item.getItemId();

        // Student fragment navigation
        if (id == R.id.student_dashboard) {
            fragmentTransaction.replace(R.id.content,new StudentDashboard());
            fragmentTransaction.commit();
            toolbar.setTitle("Dashboard");
        } else if (id == R.id.student_view_payment) {
            fragmentTransaction.replace(R.id.content,new StudentPayment());
            fragmentTransaction.commit();
            toolbar.setTitle("View Payment");
        } else if (id == R.id.student_time_table) {
            fragmentTransaction.replace(R.id.content,new StudentTimeTable());
            fragmentTransaction.commit();
            toolbar.setTitle("Time Table");
        } else if (id == R.id.student_download) {
            fragmentTransaction.replace(R.id.content,new StudentDownload());
            fragmentTransaction.commit();
            toolbar.setTitle("Download Notes");
        } else if (id == R.id.student_settings){
            fragmentTransaction.replace(R.id.content, new UserSetting());
            fragmentTransaction.commit();
            toolbar.setTitle("Setting");
        }

        // Admin fragment navigation
        else if (id == R.id.admin_dashboard){
            fragmentTransaction.replace(R.id.content,new AdminDashboard());
            fragmentTransaction.commit();
            toolbar.setTitle("Dashboard");
        } else if (id == R.id.admin_manage_class){
            fragmentTransaction.replace(R.id.content,new AdminManageClass());
            fragmentTransaction.commit();
            toolbar.setTitle("Manage Class");
        } else if (id == R.id.admin_manage_subject){
            fragmentTransaction.replace(R.id.content,new AdminManageSubject());
            fragmentTransaction.commit();
            toolbar.setTitle("Manage Subject");
        } else if (id == R.id.admin_manage_profile){
             fragmentTransaction.replace(R.id.content, new AdminManageUserProfile());
             fragmentTransaction.commit();
             toolbar.setTitle("Manage User Profile");
        } else if (id == R.id.admin_manage_payment){
            fragmentTransaction.replace(R.id.content, new AdminManagePayment());
            fragmentTransaction.commit();
            toolbar.setTitle("Manage Payment");
        } else if (id == R.id.admin_settings){
            fragmentTransaction.replace(R.id.content, new UserSetting());
            fragmentTransaction.commit();
            toolbar.setTitle("Setting");
        }

        // Teacher fragment navigation
        else if (id == R.id.teacher_dashboard) {
            fragmentTransaction.replace(R.id.content, new TeacherDashboard());
            fragmentTransaction.commit();
            toolbar.setTitle("Dashboard");
        } else if (id == R.id.teacher_upload)  {
             fragmentTransaction.replace(R.id.content, new TeacherUpload());
             fragmentTransaction.commit();
             toolbar.setTitle("Upload");
        } else if (id == R.id.teacher_timetable_attendance){
            fragmentTransaction.replace(R.id.content, new TeacherStudentAttendance());
            fragmentTransaction.commit();
            toolbar.setTitle("Timetable / Attendance");
        } else if (id == R.id.teacher_view_payment){
            fragmentTransaction.replace(R.id.content, new TeacherPayment());
            fragmentTransaction.commit();
            toolbar.setTitle("Payment");
        } else if (id == R.id.teacher_settings){
            fragmentTransaction.replace(R.id.content, new UserSetting());
            fragmentTransaction.commit();
            toolbar.setTitle("Setting");
        }

        else if (id == R.id.logout){
            showLogoutDialog();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(this);
        local_user_email = Paper.book().read(Constant.LOCAL_USER_EMAIL);
        local_user_type = Paper.book().read(Constant.LOCAL_USER_TYPE);
        local_user_img_url = Paper.book().read(Constant.LOCAL_USER_IMG_URL);
    }

    //  Set user menu
    private void setUserMenu(NavigationView navigationView){
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        if (local_user_type.equals(Constant.TYPE_STUDENT)){
            navigationView.inflateMenu(R.menu.student_drawer);
            loadirstStudentFragment();
            navigationView.getMenu().getItem(0).setChecked(true);
        } else if (local_user_type.equals(Constant.TYPE_ADMIN)){
            navigationView.inflateMenu(R.menu.admin_drawer);
            loadFirstAdminFragment();
            navigationView.getMenu().getItem(0).setChecked(true);
        } else if (local_user_type.equals(Constant.TYPE_TEACHER)){
            navigationView.inflateMenu(R.menu.teacher_drawer);
            loadFirstTeacherFragment();
            navigationView.getMenu().getItem(0).setChecked(true);
        }
    }

    private void loadirstStudentFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content,new StudentDashboard());
        fragmentTransaction.commit();
    }

    private void loadFirstAdminFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content,new AdminDashboard());
        fragmentTransaction.commit();
    }

    private void loadFirstTeacherFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content,new TeacherDashboard());
        fragmentTransaction.commit();
    }

    private void showLogoutDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
        builder.setMessage("Are you sure to logout").setTitle("Logout");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                FirebaseAuth firebaseAuth = new FirebaseService().initFirebase();
                firebaseAuth.signOut();
                Paper.book().destroy();
                finish();
                startActivity(new Intent(Home.this, Login.class));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateToken(String token) {
        FirebaseAuth firebaseAuth;
        firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_TOKEN);
        Token tokeUser = new Token(token);
        databaseReference.child(firebaseAuth.getCurrentUser().getUid()).setValue(tokeUser);
    }
}
