package com.ganeson.vigines.aplikasipengurusantuisyen.Student;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.DayService;

import java.util.ArrayList;
import java.util.List;

public class StudentTimeTable extends Fragment {

    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Day> dayList;

    public StudentTimeTable() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_student_time_table, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.rcview_student_time_table);
        linearLayoutManager = new LinearLayoutManager(StudentTimeTable.this.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = view.findViewById(R.id.tv_empty_class);
    }

    private void loadDays(){
        dayList = new ArrayList<>();
        new DayService().getAllDays(StudentTimeTable.this.getContext(), dayList, textView, recyclerView, Constant.TYPE_STUDENT);
    }
    @Override
    public void onResume() {
        super.onResume();
        loadDays();
    }

}
