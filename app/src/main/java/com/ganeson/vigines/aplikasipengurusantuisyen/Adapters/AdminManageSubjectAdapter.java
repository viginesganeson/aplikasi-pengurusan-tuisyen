package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewSubject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

import java.util.List;

public class AdminManageSubjectAdapter extends RecyclerView.Adapter<AdminManageSubjectAdapter.AdminManageSubjectViewHolder> {

    private List<Subject> subjectList;
    private Context context;

    public AdminManageSubjectAdapter(Context context, List<Subject> subjectList){
        this.subjectList = subjectList;
        this.context = context;
    }

    @NonNull
    @Override
    public AdminManageSubjectViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_admin_manage_subject,null);
        return new AdminManageSubjectViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminManageSubjectViewHolder adminManageSubjectViewHolder, int i) {
        adminManageSubjectViewHolder.tvName.setText(subjectList.get(i).getName());
        adminManageSubjectViewHolder.tvCode.setText(subjectList.get(i).getCode());
        adminManageSubjectViewHolder.tvDesc.setText(subjectList.get(i).getDesc());

        adminManageSubjectViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminViewSubject.class);
                intent.putExtra("SUBJECT_ID",subjectList.get(adminManageSubjectViewHolder.getAdapterPosition()).getId());
                intent.putExtra("SUBJECT_NAME", subjectList.get(adminManageSubjectViewHolder.getAdapterPosition()).getName());
                intent.putExtra("SUBJECT_CODE", subjectList.get(adminManageSubjectViewHolder.getAdapterPosition()).getCode());
                intent.putExtra("SUBJECT_DESC", subjectList.get(adminManageSubjectViewHolder.getAdapterPosition()).getDesc());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.subjectList.size();
    }

    class AdminManageSubjectViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName, tvCode, tvDesc;
        private CardView cardView;

        private AdminManageSubjectViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_subject_name);
            tvCode = itemView.findViewById(R.id.tv_subject_code);
            tvDesc = itemView.findViewById(R.id.tv_subject_desc);
            cardView = itemView.findViewById(R.id.rcview_admin_manage_subject_cview);
        }
    }
}
