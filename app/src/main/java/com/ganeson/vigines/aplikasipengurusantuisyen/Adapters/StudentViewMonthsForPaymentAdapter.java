package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentAddPayment;

import java.util.Calendar;
import java.util.List;

public class StudentViewMonthsForPaymentAdapter extends RecyclerView.Adapter<StudentViewMonthsForPaymentAdapter.StudentViewMonthsForPaymentViewHolder> {

    private List<Month> monthList;
    private Context context;

    public StudentViewMonthsForPaymentAdapter(Context context, List<Month> monthList){
        this.context = context;
        this.monthList = monthList;
    }

    @NonNull
    @Override
    public StudentViewMonthsForPaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_months_for_payment,null);
        return new StudentViewMonthsForPaymentViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewMonthsForPaymentViewHolder studentViewMonthsForPaymentViewHolder, final int i) {
        final int year = Calendar.getInstance().get(Calendar.YEAR);
        studentViewMonthsForPaymentViewHolder.tvMonth.setText(monthList.get(i).getMonth());
        studentViewMonthsForPaymentViewHolder.tvYear.setText(Integer.toString(year));
        studentViewMonthsForPaymentViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, StudentAddPayment.class);
                intent.putExtra("MONTH", monthList.get(i).getMonth());
                intent.putExtra("YEAR", Integer.toString(year));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return monthList.size();
    }

    public class StudentViewMonthsForPaymentViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMonth, tvYear;
        private CardView cardView;

        public StudentViewMonthsForPaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            cardView = itemView.findViewById(R.id.rcview_view_months_for_payment_cview);
        }
    }
}
