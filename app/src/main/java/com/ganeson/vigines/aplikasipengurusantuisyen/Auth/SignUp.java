package com.ganeson.vigines.aplikasipengurusantuisyen.Auth;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.AuthService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;

import java.util.Calendar;
import java.util.Locale;


public class SignUp extends AppCompatActivity {

    private Integer USER_TYPE;
    private Toolbar toolbar;
    private TextInputEditText txtName,txtIcno,txtEmail,txtPassword, txtPhone, txtDob;
    private CoordinatorLayout coordinatorLayout;
    private RadioButton radioButton;
    private RadioGroup radioGroup;
    private Button btnSignup;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        USER_TYPE = getIntent().getIntExtra("USER_TYPE", 0);

        toolbar = findViewById(R.id.tlb_signup);
        txtName = findViewById(R.id.txt_name);
        txtIcno = findViewById(R.id.txt_ic);
        txtEmail = findViewById(R.id.txt_email);
        txtPassword = findViewById(R.id.txt_password);
        txtPhone = findViewById(R.id.txt_phone);
        txtDob = findViewById(R.id.txt_dob);
        radioGroup = findViewById(R.id.rgroup_signup);
        btnSignup = findViewById(R.id.btn_signup);
        coordinatorLayout = findViewById(R.id.cdlayout_snackbar);
        radioGroup = findViewById(R.id.rgroup_signup);
        relativeLayout = findViewById(R.id.rl_signup);
        setToolbar(toolbar);

        btnSignup.setOnClickListener(this.signUp);
        txtIcno.setOnFocusChangeListener(this.focusIs);

    }

    private View.OnClickListener signUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new GlobalService().hideKeyboard(SignUp.this, relativeLayout);
            String email = null, name = null, icno = null, password = null, phone = null, dob = null, gender = null;
            int selectedId =  radioGroup.getCheckedRadioButtonId();
            radioButton = findViewById(selectedId);

            if (TextUtils.isEmpty(txtEmail.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout, "Email is required");
            } else if (TextUtils.isEmpty(txtName.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout, "Name is required");
            } else if (TextUtils.isEmpty((txtIcno.getText()))) {
                new GlobalService().showSnackBar(coordinatorLayout, "Ic Number is required");
            } else if (TextUtils.isEmpty(txtPassword.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout, "Password is required");
            } else if (TextUtils.isEmpty(txtPhone.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout, "Phone is required");
            } else if (selectedId == -1) {
                new GlobalService().showSnackBar(coordinatorLayout, "Gender is required");
            } else {

                if (txtIcno.getText().length() != 12){
                    new GlobalService().showSnackBar(coordinatorLayout, "IC No should be 12 characthers");
                } else {
                    email = txtEmail.getText().toString();
                    name = txtName.getText().toString();
                    icno = txtIcno.getText().toString();
                    password = txtPassword.getText().toString();
                    phone = txtPhone.getText().toString();
                    dob = txtDob.getText().toString();
                    gender = radioButton.getText().toString();
                    String month = Calendar.getInstance().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
                    String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
                    String signUpMonth = month + "-" + year;
                    new AuthService().signUpUser(SignUp.this,coordinatorLayout, email, password, name, icno, phone, dob, gender, USER_TYPE, signUpMonth);
                }

            }
        }
    };

    private View.OnFocusChangeListener focusIs = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (!TextUtils.isEmpty(txtIcno.getText())) {
                    String icNo = txtIcno.getText().toString();
                    String year = icNo.substring(0, 2);
                    String month = icNo.substring(2, 4);
                    String day = icNo.substring(4, 6);
                    txtDob.setText(day + "/" + month + "/" + year);
                }
            }
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }

    public void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
