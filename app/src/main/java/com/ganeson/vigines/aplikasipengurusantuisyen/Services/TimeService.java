package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.support.annotation.NonNull;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Time;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class TimeService {

    private DatabaseReference databaseReference;
    private static final int NUMBER_OF_TIMES = 7;
    private String [] times;
    public TimeService () {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_TIME);
    }

    private void addTimes(){
        times = new String[]{"T08T10", "T10T12", "T12T14", "T1416", "T16T18", "T18T20", "T20T22"};
        for (int i = 0 ; i < NUMBER_OF_TIMES; i++){
            String id = databaseReference.push().getKey();
            Time time = new Time(id, times[i]);
            if (id != null) {
                databaseReference.child(id).setValue(time)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            System.out.println(e.getMessage());
                        }
                    });
            }
        }

    }

    public void checkIfExist() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    addTimes();
                } else {
                    System.out.println("Time is available");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
