package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SubjectService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.UserService;

import java.util.ArrayList;
import java.util.List;

public class AdminDashboard extends Fragment {

    private TextView tvStudentCount, tvTeacherCount, tvClassCount, tvSubjectCount;
    private List<Class> classList;
    private List<User> userList;
    private List<Subject> subjectList;

    public AdminDashboard() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvStudentCount = view.findViewById(R.id.tv_student_count);
        tvTeacherCount = view.findViewById(R.id.tv_teacher_count);
        tvSubjectCount = view.findViewById(R.id.tv_subject_count);
        tvClassCount = view.findViewById(R.id.tv_class_count);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.loadDashboard();
    }

    private void loadDashboard() {
        classList = new ArrayList<>();
        userList = new ArrayList<>();
        subjectList = new ArrayList<>();
        new ClassService().getClassDashboardForAdmin(classList, tvClassCount);
        new UserService().getUserDashboardForAdmin(userList, tvStudentCount, Constant.TYPE_STUDENT);
        new UserService().getUserDashboardForAdmin(userList, tvTeacherCount, Constant.TYPE_TEACHER);
        new SubjectService().getSubjectDashboardForAdmin(subjectList, tvSubjectCount);


    }
}
