package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.support.annotation.NonNull;
import android.util.Log;

import com.ganeson.vigines.aplikasipengurusantuisyen.API.FireBaseAPIService;
import com.ganeson.vigines.aplikasipengurusantuisyen.API.InitAPI;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Notification;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.APIResponse;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Sender;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationService {

    private DatabaseReference databaseReference;
    private String token;
    private FireBaseAPIService fireBaseAPIService;


    public NotificationService (){
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_TOKEN);
        this.fireBaseAPIService = InitAPI.getFCMService();
    }

    public void sendNotification(String userId, final String title, final String body) {
        databaseReference.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot tokenSnapshot: dataSnapshot.getChildren()){
                    token = tokenSnapshot.getValue(String.class);
                    Log.v("NOTIFICATION", token);
                    Notification notification = new Notification(title, body);
                    Sender content = new Sender(token, notification);
                    fireBaseAPIService.sendNotification(content).enqueue(new Callback<APIResponse>() {
                        @Override
                        public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                            if (response.body() != null) {
                                if (response.body().success == 1) {
                                    Log.v("NOTIFICATION", "SUCCESS");
                                } else {
                                    Log.v("NOTIFICATION", "FAILURE");
                                    Log.v("NOTIFICATION", response.body().toString());
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<APIResponse> call, Throwable t) {
                            Log.v("NOTIFICATION", "SUCCESS");
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
