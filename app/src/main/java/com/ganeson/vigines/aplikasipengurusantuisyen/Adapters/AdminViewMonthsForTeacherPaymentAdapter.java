package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewTeacherPayment;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

import java.util.Calendar;
import java.util.List;

public class AdminViewMonthsForTeacherPaymentAdapter extends RecyclerView.Adapter<AdminViewMonthsForTeacherPaymentAdapter.AdminViewMonthsForTeacherPaymentViewHolder> {

    private List<Month> monthList;
    private Context context;
    private String teacherId;

    public AdminViewMonthsForTeacherPaymentAdapter(Context context, List<Month> monthList, String teacherId){
        this.context = context;
        this.monthList = monthList;
        this.teacherId = teacherId;
    }
    @NonNull
    @Override
    public AdminViewMonthsForTeacherPaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_months_for_payment,null);
        return new AdminViewMonthsForTeacherPaymentAdapter.AdminViewMonthsForTeacherPaymentViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminViewMonthsForTeacherPaymentViewHolder adminViewMonthsForTeacherPaymentViewHolder, int i) {
        final int year = Calendar.getInstance().get(Calendar.YEAR);
        adminViewMonthsForTeacherPaymentViewHolder.tvMonth.setText(monthList.get(i).getMonth());
        adminViewMonthsForTeacherPaymentViewHolder.tvYear.setText(Integer.toString(year));
        adminViewMonthsForTeacherPaymentViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AdminViewTeacherPayment.class);
                intent.putExtra("MONTH", monthList.get(adminViewMonthsForTeacherPaymentViewHolder.getAdapterPosition()).getMonth());
                intent.putExtra("YEAR", Integer.toString(year));
                intent.putExtra("TEACHER_ID", teacherId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return monthList.size();
    }

    class AdminViewMonthsForTeacherPaymentViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMonth, tvYear;
        private CardView cardView;
        AdminViewMonthsForTeacherPaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            cardView = itemView.findViewById(R.id.rcview_view_months_for_payment_cview);
        }
    }
}
