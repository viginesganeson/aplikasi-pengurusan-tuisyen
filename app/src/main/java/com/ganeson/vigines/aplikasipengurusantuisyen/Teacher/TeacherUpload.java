package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentDownload;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class TeacherUpload extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private TextView textView;
    private List<Class> classList;
    private String LOCAL_USER_ID;

    public TeacherUpload() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_upload, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadLocalStorageData();
        linearLayoutManager = new LinearLayoutManager(TeacherUpload.this.getContext());
        recyclerView = view.findViewById(R.id.rcview_teacher_class_notes_upload);
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = view.findViewById(R.id.tv_empty_class);
        loadClass();
    }

    private void loadClass(){
        classList = new ArrayList<>();
        new ClassService().getClassForTeacher(TeacherUpload.this.getContext(), LOCAL_USER_ID, classList, textView, recyclerView);
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(TeacherUpload.this.getContext());
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }

}
