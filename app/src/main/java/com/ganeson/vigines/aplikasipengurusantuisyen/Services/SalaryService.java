package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminViewTeacherPaymentListAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewPaymentAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SalaryService extends GlobalService {

    private DatabaseReference databaseReference;
    private Salary salary;

    public SalaryService () {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SALARY);
    }

    public void addSalary(final CoordinatorLayout coordinatorLayout, String teacherId, String month, String year, String salaryUrl){
        showSnackBar(coordinatorLayout, "Uploading file..");
        String id = databaseReference.push().getKey();
        Salary fee = new Salary(id, year, month, teacherId, salaryUrl, false);
        if (id != null) {
            databaseReference.child(id).setValue(fee)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Payment Uploded");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showSnackBar(coordinatorLayout, e.getMessage());
                    }
                });
        }
    }

    public void getSalary(final Context context, final List<Salary> salaryList, final TextView textView, final RecyclerView recyclerView, final String userId, final String month, final String year, final CoordinatorLayout coordinatorLayout, final String SOURCE_TYPE) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                salaryList.clear();
                if (!dataSnapshot.exists()){
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot salaryDataSnapshot : dataSnapshot.getChildren()) {
                        salary = salaryDataSnapshot.getValue(Salary.class);
                        if (salary != null) {
                            if (salary.getTeacherId().equals(userId) && salary.getMonth().equals(month) && salary.getYear().equals(year)){
                                salaryList.add(salary);
                            }
                        }
                    }
                }

                if (salaryList.size() > 0){
                    if (SOURCE_TYPE.equals("ADMIN_VIEW_TEACHER_PAYMENT")){
                        recyclerView.setAdapter(new AdminViewTeacherPaymentListAdapter(context, salaryList, coordinatorLayout));
                    } else if (SOURCE_TYPE.equals("TEACHER_VIEW_PAYMENT")){
                        recyclerView.setAdapter(new TeacherViewPaymentAdapter(context, salaryList, coordinatorLayout, month, year));
                    }

                } else {
                    textView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    public void deleteSalary(String id, final Context context, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Deleting files...");
        progressDialog.show();
        databaseReference.child(id).removeValue()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Files Deleted");
                        progressDialog.dismiss();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                    }
                });
    }

    public void getSalaryDashboardForTeacher(final Context context, String teacherId, final List<Salary> salaryList, final TextView textView) {
        final String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        final String month = Calendar.getInstance().getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        Log.v("dash", year + month);
        databaseReference.orderByChild("teacherId").equalTo(teacherId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                salaryList.clear();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot salaryDataSnapshot : dataSnapshot.getChildren()) {
                        salary = salaryDataSnapshot.getValue(Salary.class);
                        if (salary != null) {
                            if (salary.getMonth().equals(month) && salary.getYear().equals(year)){
                                salaryList.add(salary);
                            }
                        }
                    }
                }

                if (salaryList.size() > 0){
                    textView.setText(context.getResources().getString(R.string.paid_uppercase));
                } else {
                    textView.setText(context.getResources().getString(R.string.pending_uppercase));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
