package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Note;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.NoteService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;

import java.util.List;

public class TeacherViewNotesByClassAdapter extends RecyclerView.Adapter<TeacherViewNotesByClassAdapter.TeacherViewNotesByClassViewHolder> {

    private List<Note> noteList;
    private Context context;
    private CoordinatorLayout coordinatorLayout;

    public TeacherViewNotesByClassAdapter(Context context, List<Note> noteList, CoordinatorLayout coordinatorLayout){
        this.noteList = noteList;
        this.context = context;
        this.coordinatorLayout = coordinatorLayout;
    }

    @NonNull
    @Override
    public TeacherViewNotesByClassAdapter.TeacherViewNotesByClassViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_teacher_view_notes,null);
        TeacherViewNotesByClassAdapter.TeacherViewNotesByClassViewHolder crsv = new TeacherViewNotesByClassAdapter.TeacherViewNotesByClassViewHolder(layoutView);
        return crsv;
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherViewNotesByClassAdapter.TeacherViewNotesByClassViewHolder teacherViewNotesByClassViewHolder, final int i) {
        teacherViewNotesByClassViewHolder.tvNotesId.setText(noteList.get(i).getId());

        teacherViewNotesByClassViewHolder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder
                        .setMessage("Are you sure to delete this file ?")
                        .setTitle("Delete File")
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                new NoteService().deleteNote(noteList.get(i).getId(), context, coordinatorLayout);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }

    public class TeacherViewNotesByClassViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private TextView tvNotesId;

        public TeacherViewNotesByClassViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.rcview_teacher_view_notes_cview);
            tvNotesId = itemView.findViewById(R.id.tv_note_id);
        }
    }
}
