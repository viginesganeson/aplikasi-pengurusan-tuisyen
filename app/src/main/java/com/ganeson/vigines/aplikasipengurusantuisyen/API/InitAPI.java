package com.ganeson.vigines.aplikasipengurusantuisyen.API;

public class InitAPI {

    private static final String BASE_URL = "https://fcm.googleapis.com/";

    public static FireBaseAPIService getFCMService() {
        return RetroFitClient.getClient(BASE_URL).create(FireBaseAPIService.class);
    }
}
