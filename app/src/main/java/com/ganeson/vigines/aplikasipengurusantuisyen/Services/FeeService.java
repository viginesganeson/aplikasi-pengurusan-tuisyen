package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminViewStudentPaymentListAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.StudentViewPaymentsAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Fee;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class FeeService extends GlobalService {

    private DatabaseReference databaseReference;
    private Fee fee;

    public FeeService () {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_FEE);
    }

    public void getFees(final Context context, final List<Fee> feeList, final TextView textView, final RecyclerView recyclerView, final String userId, final String month, final String year, final CoordinatorLayout coordinatorLayout, final String SOURCE_TYPE) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                feeList.clear();
                if (!dataSnapshot.exists()){
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot feeDataSnapshot : dataSnapshot.getChildren()) {
                        fee = feeDataSnapshot.getValue(Fee.class);
                        if (fee != null) {
                            if (fee.getStudentId().equals(userId) && fee.getMonth().equals(month) && fee.getYear().equals(year)){
                                feeList.add(fee);
                            }
                        }
                    }
                }

                if (feeList.size() > 0){
                    if (SOURCE_TYPE.equals("ADMIN_VIEW_STUDENT_PAYMENT")){
                        recyclerView.setAdapter(new AdminViewStudentPaymentListAdapter(context, feeList, coordinatorLayout));
                    } else if (SOURCE_TYPE.equals("STUDENT_ADD_STUDENT_PAYMENT")) {
                        recyclerView.setAdapter(new StudentViewPaymentsAdapter(context, feeList));
                    }
                } else {
                    textView.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    public void addFes(final CoordinatorLayout coordinatorLayout, String userId, String month, String year, String feeUrl){
        showSnackBar(coordinatorLayout, "Uploading file..");
        String id = databaseReference.push().getKey();
        Fee fee = new Fee(id, year, month, userId, feeUrl, false);
        if (id != null) {
            databaseReference.child(id).setValue(fee)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Payment Uploded");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showSnackBar(coordinatorLayout, e.getMessage());
                    }
                });
        }

    }

    public void updateFee(final Context context, final String id, final CoordinatorLayout coordinatorLayout, Boolean status){
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Updating subject...");
        progressDialog.show();
        databaseReference.child(id).child("status").setValue(status)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "Fee Updated");
                    progressDialog.dismiss();

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showSnackBar(coordinatorLayout, e.getMessage());
                    progressDialog.dismiss();
                }
            });
    }
}
