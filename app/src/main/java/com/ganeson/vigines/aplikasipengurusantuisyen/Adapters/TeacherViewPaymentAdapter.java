package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;

import java.util.List;

public class TeacherViewPaymentAdapter extends RecyclerView.Adapter<TeacherViewPaymentAdapter.TeacherViewPaymentViewHolder> {

    private Context context;
    private List<Salary> salaryList;
    private CoordinatorLayout coordinatorLayout;
    private String month, year;

    public TeacherViewPaymentAdapter(Context context, List<Salary> salaryList, CoordinatorLayout coordinatorLayout, String month, String year) {
        this.context = context;
        this.salaryList = salaryList;
        this.coordinatorLayout = coordinatorLayout;
        this.month = month;
        this.year = year;
    }

    @NonNull
    @Override
    public TeacherViewPaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_payment,null);
        return new TeacherViewPaymentAdapter.TeacherViewPaymentViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherViewPaymentViewHolder teacherViewPaymentViewHolder, final int i) {
        teacherViewPaymentViewHolder.tvYear.setText(salaryList.get(i).getYear());
        teacherViewPaymentViewHolder.tvFeeId.setText(salaryList.get(i).getId());
        teacherViewPaymentViewHolder.tvMonth.setText(salaryList.get(i).getMonth());
        teacherViewPaymentViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GlobalService().showSnackBar(coordinatorLayout, "Downloading File...");
                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(salaryList.get(i).getSalaryUrl());
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir( "/", month +"_" + year + ".pdf");
                downloadManager.enqueue(request);
            }
        });
    }

    @Override
    public int getItemCount() {
        return salaryList.size();
    }

    class TeacherViewPaymentViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMonth, tvYear, tvFeeId;
        private CardView cardView;

        TeacherViewPaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            tvFeeId = itemView.findViewById(R.id.tv_payment_id);
            cardView = itemView.findViewById(R.id.rcview_view_payment_cview);
        }
    }
}
