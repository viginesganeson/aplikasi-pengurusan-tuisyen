package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminManageSubjectAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

public class SubjectService extends GlobalService {

    private DatabaseReference databaseReference;
    private Subject subject;

    public SubjectService (){
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
    }

    public void addSubject(final Context context, final CoordinatorLayout coordinatorLayout, String subjectName, String subjectCode, String subjectDesc){
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Adding subject...");
        progressDialog.show();
        String id = databaseReference.push().getKey();
        Subject subject = new Subject(id, subjectCode, subjectName, subjectDesc);

        if (id != null) {
            databaseReference.child(id).setValue(subject)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Subject Added");
                        progressDialog.dismiss();
                        new GlobalService().destroyAct(context);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showSnackBar(coordinatorLayout, e.getMessage());
                        progressDialog.dismiss();
                    }
                });
        }
    }

    public void updateSubject(final Context context, final String id, final CoordinatorLayout coordinatorLayout, String subjectName, String subjectCode, String subjectDesc){
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Updating subject...");
        progressDialog.show();
        Subject subject = new Subject(id, subjectCode, subjectName, subjectDesc);
        databaseReference.child(id).setValue(subject)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "Subject Updated");
                    progressDialog.dismiss();
                    new GlobalService().destroyAct(context);

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showSnackBar(coordinatorLayout, e.getMessage());
                    progressDialog.dismiss();
                }
            });
    }

    public void getAllSubjects(final Context context, final List<Subject> subjectList, final TextView textView, final RecyclerView recyclerView) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()){
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                }

                for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()) {
                    subject = subjectDataSnapshot.getValue(Subject.class);
                    subjectList.add(subject);
                }

                recyclerView.setAdapter(new AdminManageSubjectAdapter(context, subjectList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void deleteSubject(String id, final Context context, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Deleting subject...");
        progressDialog.show();
        databaseReference.child(id).removeValue()
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "Subject Deleted");
                    progressDialog.dismiss();
                    new GlobalService().destroyAct(context);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
    }

    public void getSubjectDashboardForAdmin(final List<Subject> subjectList, final TextView textView) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList.clear();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()) {
                        subject = subjectDataSnapshot.getValue(Subject.class);
                        if (subject != null) {
                            subjectList.add(subject);
                        }
                    }
                }

                if (subjectList.size() > 0) {
                    textView.setText(String.format(Locale.getDefault(), "%d", subjectList.size()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
