package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Fee;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FeeService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SalaryService;

import java.util.ArrayList;
import java.util.List;

public class AdminViewStudentPayment extends AppCompatActivity {

    private String STUDENT_ID, MONTH, YEAR;
    private TextView textView;
    private RecyclerView recyclerView;
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private List<Fee> feeList;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_student_payment);

        STUDENT_ID = getIntent().getStringExtra("STUDENT_ID");
        MONTH = getIntent().getStringExtra("MONTH");
        YEAR = getIntent().getStringExtra("YEAR");

        coordinatorLayout = findViewById(R.id.cl_admin_view_student_payment);
        textView = findViewById(R.id.tv_empty_payments);
        toolbar = findViewById(R.id.tlb_admin_view_student_payment);
        recyclerView = findViewById(R.id.rcview_admin_view_student_payment);
        linearLayoutManager = new LinearLayoutManager(AdminViewStudentPayment.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        setToolbar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadSalary();
    }

    private void loadSalary(){
        feeList = new ArrayList<>();
        new FeeService().getFees(AdminViewStudentPayment.this, feeList, textView, recyclerView, STUDENT_ID, MONTH, YEAR, coordinatorLayout, "ADMIN_VIEW_STUDENT_PAYMENT");
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle(MONTH + " Payment" + " (" + YEAR +")");
    }
}
