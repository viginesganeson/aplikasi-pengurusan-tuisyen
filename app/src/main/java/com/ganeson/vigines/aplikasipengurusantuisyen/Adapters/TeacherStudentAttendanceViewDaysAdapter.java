package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherViewClassByDay;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.paperdb.Paper;

public class TeacherStudentAttendanceViewDaysAdapter extends RecyclerView.Adapter<TeacherStudentAttendanceViewDaysAdapter.TeacherStudentAttendanceViewDayViewHolder> {

    private Context context;
    private List<Day> dayList;

    public TeacherStudentAttendanceViewDaysAdapter(Context context, List<Day> dayList) {
        this.context = context;
        this.dayList = dayList;
    }

    @NonNull
    @Override
    public TeacherStudentAttendanceViewDayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_teacher_student_attendance_view_days,null);
        return new TeacherStudentAttendanceViewDaysAdapter.TeacherStudentAttendanceViewDayViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherStudentAttendanceViewDayViewHolder teacherStudentAttendanceViewDayViewHolder, final int i) {
        teacherStudentAttendanceViewDayViewHolder.tvDay.setText(dayList.get(i).getDay());
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        // full name form of the day
        String today = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

        if (dayList.get(i).getDay().equals(today)){
            teacherStudentAttendanceViewDayViewHolder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.colorCardDashboard1));
        }

        teacherStudentAttendanceViewDayViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TeacherViewClassByDay.class);
                intent.putExtra("TEACHER_ID",loadLocalStorageData());
                intent.putExtra("DAY",dayList.get(i).getDay());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dayList.size();
    }

    public class TeacherStudentAttendanceViewDayViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private TextView tvDay;

        public TeacherStudentAttendanceViewDayViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cview_teacher_student_attendance_view_days);
            tvDay = itemView.findViewById(R.id.tv_day);
        }
    }

    // Load local storage data
    private String loadLocalStorageData (){
        Paper.init(this.context);
        return Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
