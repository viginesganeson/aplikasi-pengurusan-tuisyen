package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class User {

    private String id,name,ic_no,email,phone,date_of_birth,gender,imgUrl,signUpMonth;
    private Integer user_type;
    private Boolean isActive;

    //empty constructor
    public User() {}

    //constructor
    public User(String id, String name, String ic_no, String email, String phone, String date_of_birth, String gender, Integer user_type, Boolean isActive, String imgUrl, String signUpMonth) {
        this.id = id;
        this.name = name;
        this.ic_no = ic_no;
        this.email = email;
        this.phone = phone;
        this.date_of_birth = date_of_birth;
        this.gender = gender;
        this.user_type = user_type;
        this.isActive = isActive;
        this.imgUrl = imgUrl;
        this.signUpMonth = signUpMonth;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIc_no() {
        return ic_no;
    }

    public void setIc_no(String ic_no) {
        this.ic_no = ic_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getUser_type() {
        return user_type;
    }

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getSignUpMonth() {
        return signUpMonth;
    }

    public void setSignUpMonth(String signUpMonth) {
        this.signUpMonth = signUpMonth;
    }
}
