package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewDatesForAttendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Attendance;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.AttendanceService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.paperdb.Paper;

public class TeacherViewAttendanceByClass extends AppCompatActivity {

    private String CLASS_ID, SUBJECT_NAME, LOCAL_USER_ID, TODAY_DATE;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Attendance> attendanceList;
    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private FloatingActionButton floatingActionButton;
    private Attendance attendance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(Constant.LOG_ACTIVITY, "TeacherViewAttendanceByClass");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_view_attendance_by_class);
        loadLocalStorageData();
        loadTodayDate();

        CLASS_ID = getIntent().getStringExtra("CLASS_ID");
        SUBJECT_NAME = getIntent().getStringExtra("SUBJECT_NAME");

        toolbar = findViewById(R.id.tlb_teacher_view_student_by_class_for_attendance);
        recyclerView = findViewById(R.id.rcview_teacher_view_student_by_class_for_attendance);
        linearLayoutManager = new LinearLayoutManager(TeacherViewAttendanceByClass.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = findViewById(R.id.tv_empty_students);
        coordinatorLayout = findViewById(R.id.cl_teacher_view_class_by_day);
        floatingActionButton = findViewById(R.id.fab_add_attendance);
        setToolbar(toolbar, SUBJECT_NAME);
        floatingActionButton.setOnClickListener(this.addAttendace);
    }

    private View.OnClickListener addAttendace = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(TeacherViewAttendanceByClass.this, TeacherAddAttendance.class);
            intent.putExtra("CLASS_ID", CLASS_ID);
            startActivity(intent);
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar, String day){
        toolbar.setTitle(day);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadStudentsByClass();
    }

    private void loadStudentsByClass(){
        attendanceList = new ArrayList<>();
        new AttendanceService().getAttendance(TeacherViewAttendanceByClass.this, attendanceList, CLASS_ID, TODAY_DATE, textView, recyclerView, coordinatorLayout);
        checkAttendanceAlreadyAdded();
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(this);
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }

    private void loadTodayDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        TODAY_DATE = formatter.format(calendar.getTime());
    }

    private void checkAttendanceAlreadyAdded() {
        final List <Attendance> attendanceList = new ArrayList<>();
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_ATTENDANCE);
        databaseReference.orderByChild("classId").equalTo(CLASS_ID).addValueEventListener(new ValueEventListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    floatingActionButton.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot attendanceDataSnapshot : dataSnapshot.getChildren()) {
                        attendance = attendanceDataSnapshot.getValue(Attendance.class);
                        if (attendance.getDate().equals(TODAY_DATE)){
                            attendanceList.add(attendance);
                        }
                    }
                }

                if (attendanceList.size() > 0) {
                    floatingActionButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }
}
