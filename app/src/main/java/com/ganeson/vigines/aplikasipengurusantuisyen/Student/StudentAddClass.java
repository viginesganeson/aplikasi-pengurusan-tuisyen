package com.ganeson.vigines.aplikasipengurusantuisyen.Student;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.jaiselrahman.hintspinner.HintSpinner;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class StudentAddClass extends AppCompatActivity {

    private Toolbar toolbar;
    private CoordinatorLayout coordinatorLayout;
    private HintSpinner spinnerSubject, spinnerClass;
    private Button button;
    private String DAY;
    private List<Subject> subjectList;
    private List<Class> classList;
    private ArrayAdapter<String> subjectAdapter, classAdapter;
    private Subject subject;
    private Class aClass;
    private String subjectVal, classVal;
    private TextView textView;
    private LinearLayout linearLayout;
    private String LOCAL_USER_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_add_class);
        loadLocalStorageData();
        DAY = getIntent().getStringExtra("DAY");

        coordinatorLayout = findViewById(R.id.cdlayout_student_add_subject);
        linearLayout = findViewById(R.id.ll_show_class);
        spinnerSubject = findViewById(R.id.spn_subject);
        spinnerClass = findViewById(R.id.spn_class);
        button = findViewById(R.id.btn_add_class);
        toolbar = findViewById(R.id.tlb_add_class);
        textView = findViewById(R.id.tv_class_count);

        setToolbar(toolbar);
        subjectList = new ArrayList<>();
        classList = new ArrayList<>();
        subjectAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        classAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);

        populateSubject(spinnerSubject);

        spinnerSubject.setOnItemSelectedListener(this.selectSubject);
        spinnerClass.setOnItemSelectedListener(this.selectClass);

        button.setOnClickListener(this.addClass);

    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(this);
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }

    private View.OnClickListener addClass = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (TextUtils.isEmpty(classVal)){
                new GlobalService().showSnackBar(coordinatorLayout, "Class is required");
            } else if (TextUtils.isEmpty(subjectVal) || subjectVal.equals("Select Subject")){
                new GlobalService().showSnackBar(coordinatorLayout, "Subject is required");
            } else {
                new TimetableService().addClass(StudentAddClass.this, LOCAL_USER_ID, classVal, DAY, coordinatorLayout);
            }
        }
    };

    private HintSpinner.OnItemSelectedListener selectSubject = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                int selectedPosition = position - 1;
                subjectVal = subjectList.get(selectedPosition).getId();
                populateClass(spinnerClass, subjectVal);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private HintSpinner.OnItemSelectedListener selectClass = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                int selectedPosition = position - 1;
                classVal = classList.get(selectedPosition).getId();
                new GlobalService().showSnackBar(coordinatorLayout, classVal);
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Add " + DAY + " Class" );
    }

    private void populateSubject(HintSpinner spinnerSubject){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList.clear();
                subjectAdapter.clear();
                subjectAdapter.add("Select Subject");
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        subject = userDataSnapshot.getValue(Subject.class);
                        if (subject != null) {
                            subjectAdapter.add(subject.getName());
                            subjectList.add(subject);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }

        });

        spinnerSubject.setAdapter(subjectAdapter);

    }

    private void populateClass(HintSpinner spinnerClass, String subjectId){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_CLASS);
        databaseReference.orderByChild("subjectId").equalTo(subjectId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int counter = 1;
                classList.clear();
                classAdapter.clear();
                classAdapter.add("Select Class");
                for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()){
                    if (classDataSnapshot.exists()){
                        aClass = classDataSnapshot.getValue(Class.class);
                        if (aClass != null) {
                            if (aClass.getDay().equals(DAY)) {
                                classAdapter.add("Class "+ counter + " - " + aClass.getTime());
                                classList.add(aClass);
                            }
                        }
                        counter ++;
                    }
                }
                int classCount = classAdapter.getCount() - 1;
                if (classCount > 0){
                    textView.setVisibility(View.VISIBLE);
                    textView.setText("Available Class : " + classCount);
                    linearLayout.setVisibility(View.VISIBLE);
                } else {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText("No Class Available");
                    linearLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }

        });

        spinnerClass.setAdapter(classAdapter);

    }
}
