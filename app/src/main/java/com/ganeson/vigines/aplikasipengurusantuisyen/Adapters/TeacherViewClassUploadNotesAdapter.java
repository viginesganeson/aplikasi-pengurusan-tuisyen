package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherViewNotes;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TeacherViewClassUploadNotesAdapter extends RecyclerView.Adapter<TeacherViewClassUploadNotesAdapter.TeacherViewClassUploadNotesViewHolder> {

    private List<Class> classList;
    private List<Subject> subjectList;
    private Context context;
    private Subject subject;

    public TeacherViewClassUploadNotesAdapter(Context context, List<Class> classList){
        this.classList = classList;
        this.context = context;
    }

    @NonNull
    @Override
    public TeacherViewClassUploadNotesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_teacher_view_class_upload_notes,null);
        TeacherViewClassUploadNotesAdapter.TeacherViewClassUploadNotesViewHolder crsv = new TeacherViewClassUploadNotesAdapter.TeacherViewClassUploadNotesViewHolder(layoutView);
        return crsv;
    }

    @Override
    public void onBindViewHolder(@NonNull final TeacherViewClassUploadNotesViewHolder teacherViewClassUploadNotesViewHolder, final int i) {
        teacherViewClassUploadNotesViewHolder.tvDay.setText(classList.get(i).getDay());
        teacherViewClassUploadNotesViewHolder.tvTime.setText(classList.get(i).getTime());
        loadSubject(classList.get(i).getSubjectId(), teacherViewClassUploadNotesViewHolder);

        teacherViewClassUploadNotesViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String CLASS_NAME = teacherViewClassUploadNotesViewHolder.tvSubject.getText().toString();
                Intent intent = new Intent(context, TeacherViewNotes.class);
                intent.putExtra("CLASS_NAME", CLASS_NAME);
                intent.putExtra("CLASS_ID", classList.get(i).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public class TeacherViewClassUploadNotesViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDay, tvSubject, tvTime;
        private CardView cardView;

        public TeacherViewClassUploadNotesViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubject = itemView.findViewById(R.id.tv_subject_name);
            tvDay = itemView.findViewById(R.id.tv_day);
            tvTime = itemView.findViewById(R.id.tv_time);
            cardView = itemView.findViewById(R.id.rcview_teacher_view_class_upload_notes_cview);

        }
    }

    private void loadSubject(String subjectId, final TeacherViewClassUploadNotesAdapter.TeacherViewClassUploadNotesViewHolder teacherViewClassUploadNotesViewHolder){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.orderByChild("id").equalTo(subjectId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }

                if (dataSnapshot.exists()) {
                    for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()) {
                        subject = subjectDataSnapshot.getValue(Subject.class);
                        subjectList.add(subject);
                    }
                    teacherViewClassUploadNotesViewHolder.tvSubject.setText(subjectList.get(0).getName());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
