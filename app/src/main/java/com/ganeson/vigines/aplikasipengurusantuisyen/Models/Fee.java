package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Fee {

    String id, year, month, studentId, feeUrl;
    Boolean status;

    public Fee() {}

    public Fee(String id, String year, String month, String studentId, String feeUrl, Boolean status) {
        this.id = id;
        this.year = year;
        this.month = month;
        this.studentId = studentId;
        this.feeUrl = feeUrl;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getFeeUrl() {
        return feeUrl;
    }

    public void setFeeUrl(String feeUrl) {
        this.feeUrl = feeUrl;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
