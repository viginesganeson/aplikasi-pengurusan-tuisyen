package com.ganeson.vigines.aplikasipengurusantuisyen.Student;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Fee;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FeeService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.paperdb.Paper;

public class StudentAddPayment extends AppCompatActivity {

    private TextView textView;
    private FloatingActionButton floatingActionButton;
    private CoordinatorLayout coordinatorLayout;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<Fee> feeList;
    private Toolbar toolbar;
    private String YEAR, MONTH, LOCAL_USER_ID;
    private boolean FILE_STATUS = false;
    private Uri FILE_URI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_add_payment);
        loadLocalStorageData();

        MONTH = getIntent().getStringExtra("MONTH");
        YEAR = getIntent().getStringExtra("YEAR");

        coordinatorLayout = findViewById(R.id.cl_student_add_payment);
        textView = findViewById(R.id.tv_empty_payments);
        toolbar = findViewById(R.id.tlb_student_manage_payment);
        recyclerView = findViewById(R.id.rcview_student_payments);
        linearLayoutManager = new LinearLayoutManager(StudentAddPayment.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        floatingActionButton = findViewById(R.id.fab_add_payment);

        setToolbar(toolbar);
        floatingActionButton.setOnClickListener(this.addPayment);

    }

    private View.OnClickListener addPayment = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setType("application/pdf");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"Select PDF"), 1);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        loadFees();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setTitle(MONTH + " Payment" + " (" + YEAR +")");
        }
    }

    private void loadFees(){
        feeList = new ArrayList<>();
        new FeeService().getFees(getApplicationContext(), feeList, textView, recyclerView, LOCAL_USER_ID, MONTH, YEAR, coordinatorLayout, "STUDENT_ADD_STUDENT_PAYMENT");
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(this);
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            FILE_URI = data.getData();

            if(FILE_URI != null) {
                FILE_STATUS = true;
            }

            if (FILE_STATUS){
                String fileName = UUID.randomUUID().toString();
                final StorageReference feeFolder = new FirebaseService().initStorage().child(FirebaseService.STR_FEES + fileName);
                feeFolder.putFile(FILE_URI)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            feeFolder.getDownloadUrl()
                                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        new FeeService().addFes(coordinatorLayout, LOCAL_USER_ID, MONTH, YEAR, uri.toString());
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                                    }
                                });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                        }
                    });
            } else {
                new GlobalService().showSnackBar(coordinatorLayout, "No file selected");
            }
        }
    }
}
