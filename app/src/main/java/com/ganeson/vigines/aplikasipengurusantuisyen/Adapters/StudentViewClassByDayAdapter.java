package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class StudentViewClassByDayAdapter extends RecyclerView.Adapter<StudentViewClassByDayAdapter.StudentViewClassByDayViewHolder> {
    private List<Timetable> timetableList;
    private Context context;
    private Class aClass;
    private Subject subject;
    private List<Class> classList;
    private List<Subject> subjectList;
    private CoordinatorLayout coordinatorLayout;

    public StudentViewClassByDayAdapter(Context context, List<Timetable> timetableList, CoordinatorLayout coordinatorLayout){
        this.timetableList = timetableList;
        this.context = context;
        this.coordinatorLayout = coordinatorLayout;
    }

    @NonNull
    @Override
    public StudentViewClassByDayViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_student_view_class_by_day,null);
        StudentViewClassByDayAdapter.StudentViewClassByDayViewHolder crsv = new StudentViewClassByDayAdapter.StudentViewClassByDayViewHolder(layoutView);
        return crsv;
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewClassByDayViewHolder studentViewClassByDayViewHolder, final int i) {

        loadClass(timetableList.get(i).getClassID(),studentViewClassByDayViewHolder);
        studentViewClassByDayViewHolder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder
                    .setMessage("Are you sure to unregister this class ?")
                    .setTitle("Unregister Class")
                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            new TimetableService().deleteTimeTable(timetableList.get(i).getTimeTableID(), context, coordinatorLayout);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                AlertDialog dialog = builder.create();
                dialog.show();
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return timetableList.size();
    }

    public class StudentViewClassByDayViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSubjectName, tvTime;
        private CardView cardView;

        public StudentViewClassByDayViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubjectName = itemView.findViewById(R.id.tv_subject_name);
            tvTime = itemView.findViewById(R.id.tv_class_time);
            cardView = itemView.findViewById(R.id.rcview_student_view_class_by_day_cview);
        }
    }

    private void loadClass(final String classId, final StudentViewClassByDayViewHolder studentViewClassByDayViewHolder){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_CLASS);
        databaseReference.orderByChild("id").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                classList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }
                if (dataSnapshot.exists()) {
                    for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()) {
                        aClass = classDataSnapshot.getValue(Class.class);
                        classList.add(aClass);
                    }
                    studentViewClassByDayViewHolder.tvTime.setText(classList.get(0).getTime());
                    loadSubject(classList.get(0).getSubjectId(), studentViewClassByDayViewHolder);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadSubject(String subjectId, final StudentViewClassByDayViewHolder studentViewClassByDayViewHolder){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.orderByChild("id").equalTo(subjectId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList = new ArrayList<>();
                if (!dataSnapshot.exists()) {

                }

                if (dataSnapshot.exists()) {
                    for (DataSnapshot subjectDataSnapshot : dataSnapshot.getChildren()) {
                        subject = subjectDataSnapshot.getValue(Subject.class);
                        subjectList.add(subject);
                    }
                    studentViewClassByDayViewHolder.tvSubjectName.setText(subjectList.get(0).getName());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
