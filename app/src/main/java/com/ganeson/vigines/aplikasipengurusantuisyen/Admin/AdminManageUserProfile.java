package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminManageProfileViewPager;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

public class AdminManageUserProfile extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    public AdminManageUserProfile() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_manage_user_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tabLayout = view.findViewById(R.id.admin_manage_profile_tab_layout);
        viewPager = view.findViewById(R.id.admin_manage_profile_view_pager);
        viewPager.setOffscreenPageLimit(6);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }


    private void setupViewPager(ViewPager viewPager) {
        AdminManageProfileViewPager adminManageProfileViewPager = new AdminManageProfileViewPager(getChildFragmentManager());
//        adminManageProfileViewPager.addFrag(new AdminViewAllAdmins(),"ADMINS");
        adminManageProfileViewPager.addFrag(new AdminViewAllTeachers(),"TEACHERS");
        adminManageProfileViewPager.addFrag(new AdminViewAllStudents(),"STUDENTS");
//        adminManageProfileViewPager.addFrag(new AdminViewAllParents(),"PARENTS");
        viewPager.setAdapter(adminManageProfileViewPager);

    }
}
