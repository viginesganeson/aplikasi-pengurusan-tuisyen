package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Teacher.TeacherViewPayment;

import java.util.Calendar;
import java.util.List;

public class TeacherViewMonthsForPaymentAdapter extends RecyclerView.Adapter<TeacherViewMonthsForPaymentAdapter.TeacherViewMonthsForPaymentViewHolder> {

    private List<Month> monthList;
    private Context context;

    public TeacherViewMonthsForPaymentAdapter(Context context, List<Month> monthList){
        this.context = context;
        this.monthList = monthList;
    }
    @NonNull
    @Override
    public TeacherViewMonthsForPaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_months_for_payment,null);
        return new TeacherViewMonthsForPaymentAdapter.TeacherViewMonthsForPaymentViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull TeacherViewMonthsForPaymentViewHolder teacherViewMonthsForPaymentViewHolder, final int i) {
        final int year = Calendar.getInstance().get(Calendar.YEAR);
        teacherViewMonthsForPaymentViewHolder.tvMonth.setText(monthList.get(i).getMonth());
        teacherViewMonthsForPaymentViewHolder.tvYear.setText(Integer.toString(year));
        teacherViewMonthsForPaymentViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TeacherViewPayment.class);
                intent.putExtra("MONTH", monthList.get(i).getMonth());
                intent.putExtra("YEAR", Integer.toString(year));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return monthList.size();
    }

    public class TeacherViewMonthsForPaymentViewHolder extends RecyclerView.ViewHolder {
        private TextView tvMonth, tvYear;
        private CardView cardView;

        public TeacherViewMonthsForPaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            cardView = itemView.findViewById(R.id.rcview_view_months_for_payment_cview);
        }
    }
}
