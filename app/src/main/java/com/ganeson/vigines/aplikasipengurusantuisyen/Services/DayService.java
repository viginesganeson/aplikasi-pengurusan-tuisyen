package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminManageClassAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.StudentTimeTableAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherStudentAttendanceViewDaysAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class DayService {

    private DatabaseReference databaseReference;
    private static final int NUMBER_OF_DAYS = 7;
    private String [] days;
    private Day day;

    public DayService () {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_DAY);
    }

    private void addDays(){
        days = new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        for (int i = 0 ; i < NUMBER_OF_DAYS; i++){
            String id = databaseReference.push().getKey();
            Day day = new Day(id, days[i]);
            if (id != null){
                databaseReference.child(id).setValue(day)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            System.out.println(e.getMessage());
                        }
                    });
            }
        }
    }

    public void checkIfExist() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    addDays();
                } else {
                    System.out.println("Days is available");
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getAllDays(final Context context, final List<Day> dayList, final TextView textView, final RecyclerView recyclerView, final Integer userType) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()){
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);

                    for (DataSnapshot dayDataSnapshot : dataSnapshot.getChildren()) {
                        day = dayDataSnapshot.getValue(Day.class);
                        dayList.add(day);
                    }

                    if (userType.equals(Constant.TYPE_ADMIN)){
                        recyclerView.setAdapter(new AdminManageClassAdapter(context, dayList));
                    }

                    if(userType.equals(Constant.TYPE_STUDENT)){
                        recyclerView.setAdapter(new StudentTimeTableAdapter(context, dayList));
                    }

                    if(userType.equals(Constant.TYPE_TEACHER)){
                        recyclerView.setAdapter(new TeacherStudentAttendanceViewDaysAdapter(context, dayList));
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
