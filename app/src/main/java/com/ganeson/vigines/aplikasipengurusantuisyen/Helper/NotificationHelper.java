package com.ganeson.vigines.aplikasipengurusantuisyen.Helper;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.os.Build;

import com.ganeson.vigines.aplikasipengurusantuisyen.R;

public class NotificationHelper extends ContextWrapper {

    private static final String APT_CHANNEL_ID = "com.ganeson.vigines.aplikasipengurusantuisyen";
    private static final String APT_CHANNEL_NAME = "APT";
    private NotificationManager manager;

    public NotificationHelper(Context base)  {
        super(base);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createChannel() {
        NotificationChannel notificationChannel = new NotificationChannel(APT_CHANNEL_ID, APT_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableLights(false);
        notificationChannel.enableVibration(true);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        getManager().createNotificationChannel(notificationChannel);
    }

    private NotificationManager getManager() {
        if(manager == null) {
            manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        return manager;
    }

    @TargetApi(Build.VERSION_CODES.O)
    public android.app.Notification.Builder getMaiChannelNotification(String title, String body, PendingIntent pendingIntent, Uri soundUri) {
        return new android.app.Notification.Builder(getApplicationContext(),APT_CHANNEL_ID)
            .setContentIntent(pendingIntent)
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setSound(soundUri)
            .setAutoCancel(false);
    }
}
