package com.ganeson.vigines.aplikasipengurusantuisyen;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.ganeson.vigines.aplikasipengurusantuisyen.Auth.Login;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.AuthService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePassword extends AppCompatActivity {

    private TextInputEditText etOldPass, etNewPass, etConfirmNewPass;
    private Button buttonChange;
    private Toolbar toolbar;
    private CoordinatorLayout coordinatorLayout;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        relativeLayout = findViewById(R.id.rl_change_password);
        coordinatorLayout = findViewById(R.id.cl_change_password);
        toolbar = findViewById(R.id.tlb_change_password);
        etOldPass = findViewById(R.id.et_old_pass);
        etNewPass = findViewById(R.id.et_new_pass);
        etConfirmNewPass = findViewById(R.id.et_new_pass_confirm);
        buttonChange = findViewById(R.id.btn_change_pass);

        setToolbar(toolbar);
        buttonChange.setOnClickListener(this.changePass);
    }

    private View.OnClickListener changePass = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            new GlobalService().hideKeyboard(ChangePassword.this, relativeLayout);
            String oldpass = null;
            String newPass = null;
            String confirmNewPass = null;

            if (TextUtils.isEmpty(etOldPass.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout,"Old Pass Is Required");
                etOldPass.requestFocus();
            } else if (TextUtils.isEmpty(etNewPass.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout, "New Pass Is Required");
                etNewPass.requestFocus();
            } else if (TextUtils.isEmpty(etConfirmNewPass.getText())) {
                new GlobalService().showSnackBar(coordinatorLayout, "Confirm New Pass Is Required");
                etConfirmNewPass.requestFocus();
            } else {
                oldpass = etOldPass.getText().toString();
                newPass = etNewPass.getText().toString();
                confirmNewPass = etConfirmNewPass.getText().toString();
            }

            if (newPass != null && confirmNewPass != null) {
                if (!newPass.equals(confirmNewPass)) {
                    new GlobalService().showSnackBar(coordinatorLayout, "Password Mismatch");
                }
                else {
                    final FirebaseUser user;
                    user = FirebaseAuth.getInstance().getCurrentUser();
                    final String email = user.getEmail();
                    Log.v("pass", email);
                    AuthCredential credential = EmailAuthProvider.getCredential(email,oldpass);
                    final String tempPass = newPass;
                    user.reauthenticate(credential)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()){
                                    user.updatePassword(tempPass)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                new GlobalService().showSnackBar(coordinatorLayout, "Password Updated ");
                                                new GlobalService().destroyAct(ChangePassword.this);
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                                            }
                                        });
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                            }
                        });
                }
            }

        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
