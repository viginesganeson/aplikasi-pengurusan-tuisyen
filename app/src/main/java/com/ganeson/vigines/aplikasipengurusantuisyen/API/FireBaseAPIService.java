package com.ganeson.vigines.aplikasipengurusantuisyen.API;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.APIResponse;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Sender;

import retrofit2.http.Headers;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FireBaseAPIService {

    @Headers({
        "Content-Type:application/json",
        "Authorization:key=AAAAkdkPVZM:APA91bGxIyNwn74y6vDf5VvCUBdnlOYUf5MZ-PRzKWQ6TgWofuHWaSgw56KTC0ERQPxJClnOoU5nNU_aW6OIlGWlmgOL8Iw50HPb9gfZysgCShdXR7_KvHhNiXV1WIVbXjp2YC0sIaJc"
    })
    @POST("fcm/send")
    Call<APIResponse> sendNotification(@Body Sender body);
}