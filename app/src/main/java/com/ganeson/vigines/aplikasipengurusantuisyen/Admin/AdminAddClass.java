package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Time;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.jaiselrahman.hintspinner.HintSpinner;

import java.util.ArrayList;
import java.util.List;

public class AdminAddClass extends AppCompatActivity {

    private Toolbar toolbar;
    private CoordinatorLayout coordinatorLayout;
    private HintSpinner spinnerTeacher, spinnerPlace, spinnerTime, spinnerSubject;
    private Button button;
    private List<User> userList;
    private List<Subject> subjectList;
    private List<Time> timeList;
    private User user;
    private Subject subject;
    private Day day;
    private Time time;
    private ArrayAdapter<String> userAdapter, subjectAdapter, timeAdapter, dayAdapter;
    private String placeVal, teacherVal, subjectVal, timeVal;
    private String DAY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_class);

        DAY = getIntent().getStringExtra("DAY");

        coordinatorLayout = findViewById(R.id.cdlayout_admin_add_subject);
        spinnerTeacher = findViewById(R.id.spn_teacher);
        spinnerPlace = findViewById(R.id.spn_place);
        spinnerTime = findViewById(R.id.spn_time);
        spinnerSubject = findViewById(R.id.spn_subject);
        button = findViewById(R.id.btn_add_class);
        toolbar = findViewById(R.id.tlb_add_class);

        userAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        subjectAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        dayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        timeAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        userList = new ArrayList<>();
        subjectList = new ArrayList<>();
        timeList = new ArrayList<>();

        setToolbar(toolbar);
        populateTeacher(spinnerTeacher);
        populatePlace(spinnerPlace);
        populateSubject(spinnerSubject);
        populateTime(spinnerTime);

        button.setOnClickListener(this.addClass);
        spinnerTeacher.setOnItemSelectedListener(this.selectTeacher);
        spinnerSubject.setOnItemSelectedListener(this.selectSubject);
        spinnerPlace.setOnItemSelectedListener(this.selectPlace);
        spinnerTime.setOnItemSelectedListener(this.selectTime);
    }

    private View.OnClickListener addClass = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (TextUtils.isEmpty(placeVal)){
                new GlobalService().showSnackBar(coordinatorLayout, "Place is required");
            } else if (TextUtils.isEmpty(timeVal) || timeVal.equals("Select Time")){
                new GlobalService().showSnackBar(coordinatorLayout, "Time is required");
            } else if (TextUtils.isEmpty(teacherVal) || teacherVal.equals("Select Teacher")){
                new GlobalService().showSnackBar(coordinatorLayout, "Teacher is required");
            } else if (TextUtils.isEmpty(subjectVal) || subjectVal.equals("Select Subject")){
                new GlobalService().showSnackBar(coordinatorLayout, "Subject is required");
            } else {
                new ClassService().addClass(AdminAddClass.this, teacherVal, subjectVal, DAY, timeVal, placeVal, coordinatorLayout);
            }
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Add " + DAY + " Class" );
    }

    private void populatePlace(HintSpinner spinnerPlace){
        List<String> listPlace = new ArrayList<>();
        listPlace.add("B1");
        listPlace.add("B2");
        listPlace.add("B3");

        ArrayAdapter placeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, listPlace);
        spinnerPlace.setAdapter(placeAdapter);
    }

    private void populateTime(HintSpinner spinnerTime){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_TIME);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                timeList.clear();
                timeAdapter.clear();
                timeAdapter.add("Select Time");
                for (DataSnapshot timeDataSnapshot : dataSnapshot.getChildren()){
                    if (timeDataSnapshot.exists()){
                        time = timeDataSnapshot.getValue(Time.class);
                        if (time != null) {
                            timeAdapter.add(time.getTime());
                            timeList.add(time);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });

        spinnerTime.setAdapter(timeAdapter);
    }

    private void populateSubject(HintSpinner spinnerSubject){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList.clear();
                subjectAdapter.clear();
                subjectAdapter.add("Select Subject");
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        subject = userDataSnapshot.getValue(Subject.class);
                        if (subject != null) {
                            subjectAdapter.add(subject.getName());
                            subjectList.add(subject);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }

        });

        spinnerSubject.setAdapter(subjectAdapter);
    }

    private void populateTeacher(HintSpinner spinnerTeacher){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
        databaseReference.orderByChild("user_type").equalTo(Constant.TYPE_TEACHER).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                userAdapter.clear();
                userAdapter.add("Select Teacher");
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        user = userDataSnapshot.getValue(User.class);
                        if (user != null) {
                            userAdapter.add(user.getName());
                            userList.add(user);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }

        });

        spinnerTeacher.setAdapter(userAdapter);
    }

    private HintSpinner.OnItemSelectedListener selectTeacher = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0){
                int selectedPosition = position - 1;
                teacherVal = userList.get(selectedPosition).getId();
            }

        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private HintSpinner.OnItemSelectedListener selectSubject = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position != 0) {
                int selectedPosition = position - 1;
                subjectVal = subjectList.get(selectedPosition).getId();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private HintSpinner.OnItemSelectedListener selectPlace = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            placeVal = spinnerPlace.getSelectedItem().toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private HintSpinner.OnItemSelectedListener selectTime = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            timeVal = spinnerTime.getSelectedItem().toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

}
