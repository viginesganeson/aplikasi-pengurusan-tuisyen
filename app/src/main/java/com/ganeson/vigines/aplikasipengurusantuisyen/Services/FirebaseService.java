package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class FirebaseService {

    // DB reference
    public static String DBR_USER = "User";
    public static String DBR_SUBJECT = "Subject";
    public static String DBR_CLASS = "Class";
    static String DBR_SALARY = "Salary";
    public static String DBR_DAY = "Day";
    public static String DBR_TIME = "Time";
    public static String DBR_TIME_TABLE = "Timetable";
    static String DBR_NOTE = "Note";
    static String DBR_MONTH = "Month";
    static String DBR_FEE = "Fee";
    public static String DBR_ATTENDANCE = "Attendance";
    public static String DBR_TOKEN = "Token";

    // Storage reference
    public static String STR_NOTES = "notes/";
    public static String STR_FEES = "fees/";
    public static String STR_SALARY = "salary/";
    public static String STR_USER_PROFILE = "user-profile/";


    //init firebase auth instance
    public FirebaseAuth initFirebase() {
        return FirebaseAuth.getInstance();
    }

    //init firebase database instance
    public DatabaseReference initDatabase(String dbReference){
        return FirebaseDatabase.getInstance().getReference(dbReference);
    }

    //init firebase storage instance
    public StorageReference initStorage() {
        return FirebaseStorage.getInstance().getReference();
    }
}
