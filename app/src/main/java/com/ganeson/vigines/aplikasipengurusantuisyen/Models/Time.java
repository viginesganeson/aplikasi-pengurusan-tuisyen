package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Time {

    String id, time;

    public Time() {}

    public Time(String id, String time) {
        this.id = id;
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
