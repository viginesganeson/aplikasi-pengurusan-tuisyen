package com.ganeson.vigines.aplikasipengurusantuisyen.Student;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class StudentViewClassByDay extends AppCompatActivity {

    private Toolbar toolbar;
    private String day;
    private FloatingActionButton floatingActionButton;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Timetable> timetableList;
    private String LOCAL_USER_ID;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_view_class_by_day);
        loadLocalStorageData();
        Intent intent = getIntent();
        day = intent.getStringExtra("DAY");

        toolbar = findViewById(R.id.tlb_student_view_class_by_day);
        recyclerView = findViewById(R.id.rcview_student_view_class_by_day);
        linearLayoutManager = new LinearLayoutManager(StudentViewClassByDay.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = findViewById(R.id.tv_empty_class);
        floatingActionButton = findViewById(R.id.fab_add_class);
        coordinatorLayout = findViewById(R.id.cl_student_view_class_by_day);

        floatingActionButton.setOnClickListener(this.addClass);
        setToolbar(toolbar, day);
    }

    private View.OnClickListener addClass = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(StudentViewClassByDay.this, StudentAddClass.class);
            intent.putExtra("DAY", day);
            startActivity(intent);
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar, String day){
        toolbar.setTitle(day);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadClass();
    }

    private void loadClass(){
        timetableList = new ArrayList<>();
        new TimetableService().getClassByDay(StudentViewClassByDay.this, LOCAL_USER_ID, day, timetableList, textView, recyclerView, coordinatorLayout);
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(this);
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
