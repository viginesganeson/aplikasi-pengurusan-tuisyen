package com.ganeson.vigines.aplikasipengurusantuisyen.Auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

@SuppressWarnings("FieldCanBeLocal")
public class SignUpStaging extends AppCompatActivity {

    private Toolbar toolbar;
    private Button btnStudent,btnParent,btnStaff,btnTeacher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_staging);

        toolbar = findViewById(R.id.tlb_signupstaging);
        btnParent = findViewById(R.id.btn_parent);
        btnStaff = findViewById(R.id.btn_staff);
        btnTeacher = findViewById(R.id.btn_teacher);
        btnStudent = findViewById(R.id.btn_student);

        btnStudent.setOnClickListener(this.navigateStudent);
        btnTeacher.setOnClickListener(this.navigateTeacher);
        btnStaff.setOnClickListener(this.navigateStaff);
        btnParent.setOnClickListener(this.navigateParent);
        setToolbar(toolbar);
    }

    private View.OnClickListener navigateStudent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), SignUp.class);
            intent.putExtra("USER_TYPE", Constant.TYPE_STUDENT);
            startActivity(intent);
        }
    };

    private View.OnClickListener navigateTeacher  = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), SignUp.class);
            intent.putExtra("USER_TYPE", Constant.TYPE_TEACHER);
            startActivity(intent);
        }
    };

    private View.OnClickListener navigateStaff = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), SignUp.class);
            intent.putExtra("USER_TYPE", Constant.TYPE_ADMIN);
            startActivity(intent);
        }
    };

    private View.OnClickListener navigateParent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), SignUp.class);
            intent.putExtra("USER_TYPE", Constant.TYPE_PARENT);
            startActivity(intent);
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), Login.class));
        finish();
    }

    public void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
}
