package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminViewStudentPaymentAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class AdminViewAllStudentFee extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<User> userList;
    private User user;
    private SearchView searchView;
    private AdminViewStudentPaymentAdapter adminViewStudentPaymentAdapter;

    public AdminViewAllStudentFee() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_view_all_student_fee, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.rcview_admin_view_all_student_fee);
        linearLayoutManager = new LinearLayoutManager(AdminViewAllStudentFee.this.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadUsers();
    }

    private void loadUsers(){
        userList = new ArrayList<>();
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
        databaseReference.orderByChild("user_type").equalTo(Constant.TYPE_STUDENT).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()) {
                    user = userDataSnapshot.getValue(User.class);
                    userList.add(user);
                }

                adminViewStudentPaymentAdapter = new AdminViewStudentPaymentAdapter(AdminViewAllStudentFee.this.getContext(), userList);
                recyclerView.setAdapter(adminViewStudentPaymentAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.home, menu);
        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setVisibility(View.VISIBLE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                adminViewStudentPaymentAdapter.getFilter().filter(s);
                return false;
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        searchView.setVisibility(View.GONE);
    }
}
