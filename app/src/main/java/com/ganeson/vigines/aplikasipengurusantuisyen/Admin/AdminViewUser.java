package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.UserService;
import com.github.abdularis.civ.CircleImageView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class AdminViewUser extends AppCompatActivity {

    private Toolbar toolbar;
    private String USER_ID, USER_STATUS;
    private TextInputEditText etName, etEmail, etPhone, etIc;
    private Button btnUpdate;
    private User user;
    private CoordinatorLayout coordinatorLayout;
    private Boolean SET_STATUS = true;
    private CircleImageView circleImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_user);

        Intent intent = getIntent();
        USER_ID = intent.getStringExtra("USER_ID");
        USER_STATUS = intent.getStringExtra("USER_STATUS");

        circleImageView = findViewById(R.id.cimv_user_image);
        coordinatorLayout = findViewById(R.id.cl_admin_view_user);
        toolbar = findViewById(R.id.tlb_view_user);
        etName = findViewById(R.id.et_user_name);
        etEmail = findViewById(R.id.et_user_email);
        etPhone = findViewById(R.id.et_user_phone);
        etIc = findViewById(R.id.et_user_icno);
        btnUpdate = findViewById(R.id.btn_update_user);
        btnUpdate.setText("UNBLOCK USER");
        if (USER_STATUS.equals("true")){
            btnUpdate.setText("BLOCK USER");
            SET_STATUS = false;
        }
        setToolbar(toolbar);

        btnUpdate.setOnClickListener(this.updateUser);

    }

    private View.OnClickListener updateUser = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new UserService().updateUserStatus(AdminViewUser.this, SET_STATUS, USER_ID, coordinatorLayout );
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        populateUser();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void populateUser() {
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
        databaseReference.orderByChild("id").equalTo(USER_ID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        user = userDataSnapshot.getValue(User.class);
                        if (user != null) {
                            etName.setText(user.getName());
                            etEmail.setText(user.getEmail());
                            etPhone.setText(user.getPhone());
                            etIc.setText(user.getIc_no());
                            Picasso.get().load(user.getImgUrl()).into(circleImageView);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }
}
