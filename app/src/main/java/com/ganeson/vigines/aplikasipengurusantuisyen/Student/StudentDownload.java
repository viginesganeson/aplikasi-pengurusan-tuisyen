package com.ganeson.vigines.aplikasipengurusantuisyen.Student;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Timetable;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.TimetableService;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class StudentDownload extends Fragment {

    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Timetable> timetableList;
    String LOCAL_USER_ID;

    public StudentDownload() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_student_download, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadLocalStorageData();
        linearLayoutManager = new LinearLayoutManager(StudentDownload.this.getContext());
        recyclerView = view.findViewById(R.id.rcview_student_class_notes_download);
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = view.findViewById(R.id.tv_empty_class);
        loadClass();

    }

    private void loadClass(){
        timetableList = new ArrayList<>();
        new TimetableService().getClassByStudent(StudentDownload.this.getContext(), LOCAL_USER_ID, timetableList, textView, recyclerView);
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(StudentDownload.this.getContext());
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
