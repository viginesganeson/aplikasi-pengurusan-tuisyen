package com.ganeson.vigines.aplikasipengurusantuisyen.Student;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewClassByDay;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Note;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.NoteService;

import java.util.ArrayList;
import java.util.List;

public class StudentViewNotesByClass extends AppCompatActivity {

    private Toolbar toolbar;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Note> noteList;
    private String CLASS_ID, SUBJECT_NAME;
    private CoordinatorLayout coordinatorLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_view_notes_by_class);

        Intent intent = getIntent();
        CLASS_ID = intent.getStringExtra("CLASS_ID");
        SUBJECT_NAME = intent.getStringExtra("SUBJECT_NAME");
        Toast.makeText(getApplicationContext(), CLASS_ID, Toast.LENGTH_SHORT).show();
        toolbar = findViewById(R.id.tlb_view_notes_by_class);
        recyclerView = findViewById(R.id.rcview_student_view_notes_by_class);
        linearLayoutManager = new LinearLayoutManager(StudentViewNotesByClass.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = findViewById(R.id.tv_empty_notes);
        coordinatorLayout = findViewById(R.id.cl_student_view_notes_by_class);
        setToolbar(toolbar, SUBJECT_NAME);
        loadNotes();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar, String subjectName){
        toolbar.setTitle(subjectName);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void loadNotes(){
        noteList = new ArrayList<>();
        new NoteService().getNotesByClassToDownload(StudentViewNotesByClass.this, CLASS_ID, noteList, recyclerView, textView, coordinatorLayout);
    }
}
