package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SubjectService;

import java.util.ArrayList;
import java.util.List;


public class AdminManageSubject extends Fragment {

    FloatingActionButton floatingActionButton;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    TextView textView;
    List<Subject> subjectList;

    public AdminManageSubject() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_manage_subject, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        floatingActionButton = view.findViewById(R.id.fab_add_subject);
        floatingActionButton.setOnClickListener(this.openDialog);
        recyclerView = view.findViewById(R.id.rcview_admin_manage_subject);
        linearLayoutManager = new LinearLayoutManager(AdminManageSubject.this.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        textView = view.findViewById(R.id.tv_empty_subjects);
    }

    private View.OnClickListener openDialog = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            startActivity(new Intent(AdminManageSubject.this.getContext(), AdminAddSubject.class));
        }
    };

    private void loadSubjects(){
        subjectList = new ArrayList<>();
        new SubjectService().getAllSubjects(AdminManageSubject.this.getContext(), subjectList, textView, recyclerView);
    }
    @Override
    public void onResume() {
        super.onResume();
        loadSubjects();

    }
}
