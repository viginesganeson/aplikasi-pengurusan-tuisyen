package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Month;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.MonthService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Student.StudentPayment;

import java.util.ArrayList;
import java.util.List;


public class TeacherPayment extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<Month> monthList;

    public TeacherPayment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.rcview_teacher_payment);
        linearLayoutManager = new LinearLayoutManager(TeacherPayment.this.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void loadMonths(){
        monthList = new ArrayList<>();
        new MonthService().getAllMonths(TeacherPayment.this.getContext(), monthList, recyclerView, Constant.TYPE_TEACHER, null, null);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMonths();
    }
}
