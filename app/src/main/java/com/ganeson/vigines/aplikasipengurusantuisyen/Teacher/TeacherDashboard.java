package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Salary;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SalaryService;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class TeacherDashboard extends Fragment {

    private TextView tvPaymentStatus, tvClassCount;
    private String LOCAL_USER_ID;
    private List<Class> classList;
    private List<Salary> salaryList;

    public TeacherDashboard() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadLocalStorageData();

        tvPaymentStatus = view.findViewById(R.id.tv_payment_status);
        tvClassCount = view.findViewById(R.id.tv_class_count);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.loadDashboard();
    }

    private void loadDashboard() {
        classList = new ArrayList<>();
        salaryList = new ArrayList<>();
        new ClassService().getClassDashboardForTeacher(LOCAL_USER_ID, classList, tvClassCount);
        new SalaryService().getSalaryDashboardForTeacher(TeacherDashboard.this.getContext(),LOCAL_USER_ID, salaryList, tvPaymentStatus);
    }

    // Load local storage data
    private void loadLocalStorageData (){
        Paper.init(TeacherDashboard.this.getContext());
        LOCAL_USER_ID = Paper.book().read(Constant.LOCAL_USER_ID);
    }
}
