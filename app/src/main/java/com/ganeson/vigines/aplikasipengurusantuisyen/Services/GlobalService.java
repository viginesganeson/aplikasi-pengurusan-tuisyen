package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;

import io.paperdb.Paper;

public class GlobalService {

    //show snackbar
    public void showSnackBar(CoordinatorLayout coordinatorLayout, String text){
        Snackbar.make(coordinatorLayout,text,Snackbar.LENGTH_LONG).show();
    }

    //show progress dialog
    public ProgressDialog showProgressDialog(Context context, String message){
        ProgressDialog progressDialog =  new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(message);
        return progressDialog;
    }
    //write local storage
    void writeLocalStorage(String key, Object value){
        Paper.book().write(key, value);
    }

    //destroy act
    void destroyActivity(Context context){
        ((Activity)context).finish();
    }

    //destroy act in 2 seconds
    public void destroyAct(final Context context){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                ((Activity)context).finish();
            }
        }, 200);
    }

    // hide keyboard
    public void hideKeyboard(Context context, RelativeLayout relativeLayout) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
        }
    }
}
