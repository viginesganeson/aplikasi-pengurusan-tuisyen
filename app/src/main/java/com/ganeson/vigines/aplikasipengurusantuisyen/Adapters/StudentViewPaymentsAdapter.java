package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Fee;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

import java.util.List;

public class StudentViewPaymentsAdapter extends RecyclerView.Adapter<StudentViewPaymentsAdapter.StudentViewPaymentsViewHolder> {

    private List<Fee> feeList;
    private Context context;

    public StudentViewPaymentsAdapter(Context context, List<Fee> feeList){
        this.context = context;
        this.feeList = feeList;
    }

    @NonNull
    @Override
    public StudentViewPaymentsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_payment,null);
        return new StudentViewPaymentsAdapter.StudentViewPaymentsViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentViewPaymentsViewHolder studentViewPaymentsViewHolder, final int i) {
        studentViewPaymentsViewHolder.tvYear.setText(feeList.get(i).getYear());
        studentViewPaymentsViewHolder.tvFeeId.setText(feeList.get(i).getId());
        studentViewPaymentsViewHolder.tvMonth.setText(feeList.get(i).getMonth());

        if (feeList.get(i).getStatus()){
            studentViewPaymentsViewHolder.tvStatusApproved.setVisibility(View.VISIBLE);
        } else  {
            studentViewPaymentsViewHolder.tvStatusUnapproved.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return feeList.size();
    }

    public class StudentViewPaymentsViewHolder extends RecyclerView.ViewHolder {

        private TextView tvMonth, tvYear, tvFeeId, tvStatusUnapproved, tvStatusApproved;
        private CardView cardView;

        public StudentViewPaymentsViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonth = itemView.findViewById(R.id.tv_month);
            tvYear = itemView.findViewById(R.id.tv_year);
            tvFeeId = itemView.findViewById(R.id.tv_payment_id);
            cardView = itemView.findViewById(R.id.rcview_view_payment_cview);
            tvStatusUnapproved = itemView.findViewById(R.id.tv_status_unapproved);
            tvStatusApproved = itemView.findViewById(R.id.tv_status_approved);
        }
    }
}
