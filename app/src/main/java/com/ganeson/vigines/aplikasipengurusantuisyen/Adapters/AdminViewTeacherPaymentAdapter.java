package com.ganeson.vigines.aplikasipengurusantuisyen.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewStudentSalaryByMonth;
import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewTeacherSalaryByMonth;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;

import java.util.ArrayList;
import java.util.List;

public class AdminViewTeacherPaymentAdapter extends RecyclerView.Adapter<AdminViewTeacherPaymentAdapter.AdminViewTeacherPaymentViewHolder> implements Filterable {

    private List<User> userList;
    private Context context;
    private List<User> fullUserList;

    public AdminViewTeacherPaymentAdapter(Context context, List<User> userList){
        this.userList = userList;
        this.context = context;
        this.fullUserList = new ArrayList<>(userList);
    }

    @NonNull
    @Override
    public AdminViewTeacherPaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layoutView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_user_view_payment,null);
        return new AdminViewTeacherPaymentAdapter.AdminViewTeacherPaymentViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull AdminViewTeacherPaymentViewHolder adminViewTeacherPaymentViewHolder, final int i) {
        adminViewTeacherPaymentViewHolder.tvUserName.setText(userList.get(i).getName());
        adminViewTeacherPaymentViewHolder.tvUserPhone.setText(userList.get(i).getPhone());
        adminViewTeacherPaymentViewHolder.tvUserEmail.setText(userList.get(i).getEmail());
        if (userList.get(i).getActive()){
            adminViewTeacherPaymentViewHolder.tvuserActive.setVisibility(View.VISIBLE);
        } else {
            adminViewTeacherPaymentViewHolder.tvUserInactive.setVisibility(View.VISIBLE);
        }

        adminViewTeacherPaymentViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if (userList.get(i).getActive()){
                Intent intent = new Intent(context, AdminViewStudentSalaryByMonth.class);
                intent.putExtra("USER_ID", userList.get(i).getId());
                intent.putExtra("USER_STATUS", userList.get(i).getActive().toString());
                context.startActivity(intent);
            }
            }
        });

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class AdminViewTeacherPaymentViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUserName, tvUserEmail, tvUserPhone, tvuserActive, tvUserInactive;
        private CardView cardView;
        public AdminViewTeacherPaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            tvUserEmail = itemView.findViewById(R.id.tv_user_email);
            tvUserName = itemView.findViewById(R.id.tv_user_name);
            tvUserPhone = itemView.findViewById(R.id.tv_user_phone);
            cardView = itemView.findViewById(R.id.rcview_user_view_payment_cview);
            tvuserActive = itemView.findViewById(R.id.tv_user_active);
            tvUserInactive = itemView.findViewById(R.id.tv_user_inactive);
        }
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<User> filteredUserList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredUserList.addAll(fullUserList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (User user : fullUserList) {
                    if (user.getName().toLowerCase().contains(filterPattern)) {
                        filteredUserList.add(user);
                    }
                }
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredUserList;

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            userList.clear();
            userList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    @Override
    public Filter getFilter() {
        return filter;
    }
}
