package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Class {

    private String id, place, time, day, teacherId, subjectId;

    public Class() {}

    public Class(String id, String place, String time, String day, String teacherId, String subjectId) {
        this.id = id;
        this.place = place;
        this.time = time;
        this.day = day;
        this.teacherId = teacherId;
        this.subjectId = subjectId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }
}
