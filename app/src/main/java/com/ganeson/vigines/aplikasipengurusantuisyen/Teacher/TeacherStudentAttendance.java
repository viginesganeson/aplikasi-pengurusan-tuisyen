package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.DayService;

import java.util.ArrayList;
import java.util.List;

public class TeacherStudentAttendance extends Fragment {

    RecyclerView recyclerView;
    List<Day> dayList;
    LinearLayoutManager linearLayoutManager;
    TextView textView;

    public TeacherStudentAttendance() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_teacher_student_attendance, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textView = view.findViewById(R.id.tv_empty_days);
        recyclerView = view.findViewById(R.id.rcview_teacher_student_attendance);
        linearLayoutManager = new LinearLayoutManager(TeacherStudentAttendance.this.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void loadDays(){
        dayList = new ArrayList<>();
        new DayService().getAllDays(TeacherStudentAttendance.this.getContext(), dayList, textView, recyclerView, Constant.TYPE_TEACHER);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDays();
    }
}
