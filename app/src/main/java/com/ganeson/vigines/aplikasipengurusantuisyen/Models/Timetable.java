package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Timetable {

    private String timeTableID, studentID, classID, day;

    public Timetable(){}

    public Timetable(String timeTableID, String studentID, String classID, String day) {
        this.timeTableID = timeTableID;
        this.studentID = studentID;
        this.classID = classID;
        this.day = day;
    }

    public String getTimeTableID() {
        return timeTableID;
    }

    public void setTimeTableID(String timeTableID) {
        this.timeTableID = timeTableID;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}

