package com.ganeson.vigines.aplikasipengurusantuisyen.Models;

public class Attendance {

    private String id, studentId, classId, date;

    public Attendance() {}

    public Attendance(String id, String studentId, String classId, String date) {
        this.id = id;
        this.studentId = studentId;
        this.classId = classId;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
