package com.ganeson.vigines.aplikasipengurusantuisyen.Admin;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Constant;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Day;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Place;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Subject;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Time;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.User;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.SubjectService;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.jaiselrahman.hintspinner.HintSpinner;
import com.jaiselrahman.hintspinner.HintSpinnerAdapter;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.util.ArrayList;
import java.util.List;

public class AdminViewClass extends AppCompatActivity {

    private Toolbar toolbar;
    private Button button;
    private List<User> userList;
    private List<Subject> subjectList;
    private List<Day> dayList;
    private List<Time> timeList;
    private List<Class> classList;
    private User user;
    private Subject subject;
    private Day day;
    private Time time;
    private Class aClass;
    private ArrayAdapter<String> userAdapter, subjectAdapter, timeAdapter, dayAdapter;
    private CoordinatorLayout coordinatorLayout;
    private String dayVal, placeVal, teacherVal, subjectVal, timeVal;
    private AutoCompleteTextView autoCompleteTextView;
    private HintSpinner spnDay, spnTeacher, spnPlace, spnTime, spnSubject;
    private String CLASS_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_class);

        Intent intent = getIntent();
        CLASS_ID = intent.getStringExtra("CLASS_ID");

        toolbar = findViewById(R.id.tlb_view_class);
        spnDay = findViewById(R.id.spn_day);
        spnTime = findViewById(R.id.spn_time);
        spnTeacher = findViewById(R.id.spn_teacher);
        spnPlace = findViewById(R.id.spn_place);
        spnSubject = findViewById(R.id.spn_subject);
        coordinatorLayout = findViewById(R.id.cdlayout_admin_view_class);

        spnDay = findViewById(R.id.spn_day);

        button = findViewById(R.id.btn_update_class);

        userAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        subjectAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        dayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        timeAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1);
        userList = new ArrayList<>();
        subjectList = new ArrayList<>();
        dayList = new ArrayList<>();
        timeList = new ArrayList<>();
        classList = new ArrayList<>();

        setToolbar(toolbar);
        populateDays(spnDay);
        populateTeacher(spnTeacher);
        populatePlace(spnPlace);
        populateSubject(spnSubject);
        populateTime(spnTime);


        setToolbar(toolbar);
        getClassById(CLASS_ID);

        button.setOnClickListener(this.updateClass);

        spnDay.setOnItemSelectedListener(this.selectDay);
        spnTeacher.setOnItemSelectedListener(this.selectTeacher);
        spnPlace.setOnItemSelectedListener(this.selectPlace);
        spnSubject.setOnItemSelectedListener(this.selectSubject);
        spnTime.setOnItemSelectedListener(this.selectTime);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setToolbar(Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //BUTTON ON CLICK LISTENER
    private View.OnClickListener updateClass = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (TextUtils.isEmpty(dayVal)){
              new GlobalService().showSnackBar(coordinatorLayout, "Day is required");
            } else if (TextUtils.isEmpty(placeVal)){
                new GlobalService().showSnackBar(coordinatorLayout, "Place is required");
            } else if (TextUtils.isEmpty(timeVal) || timeVal.equals("Select Time")){
                new GlobalService().showSnackBar(coordinatorLayout, "Time is required");
            } else if (TextUtils.isEmpty(teacherVal) || teacherVal.equals("Select Teacher")){
                new GlobalService().showSnackBar(coordinatorLayout, "Teacher is required");
            } else if (TextUtils.isEmpty(subjectVal) || subjectVal.equals("Select Subject")){
                new GlobalService().showSnackBar(coordinatorLayout, "Subject is required");
            } else {
                new ClassService().updateClass(AdminViewClass.this, CLASS_ID, teacherVal, subjectVal, dayVal, timeVal, placeVal, coordinatorLayout);
            }

        }
    };

    // SPINNER DAY ON ITEM SELECTED LISTENER
    private HintSpinner.OnItemSelectedListener selectDay = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            dayVal = spnDay.getSelectedItem().toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }

    };

    // SPINNER TEACHER ON ITEM SELECTED LISTENER
    private HintSpinner.OnItemSelectedListener selectTeacher = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            teacherVal = userList.get(position).getId();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    // SPINNER TIME ON ITEM SELECTED LISTENER
    private HintSpinner.OnItemSelectedListener selectTime = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            timeVal = spnTime.getSelectedItem().toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    // SPINNER PLACE ON ITEM SELECTED LISTENER
    private HintSpinner.OnItemSelectedListener selectPlace = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            placeVal = spnPlace.getSelectedItem().toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    // SPINNER SUBJECT ON ITEM SELECTED LISTENER
    private HintSpinner.OnItemSelectedListener selectSubject = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            subjectVal = subjectList.get(position).getId();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void getClassById(String classId){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_CLASS);
        databaseReference.orderByChild("id").equalTo(classId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                classList.clear();
                for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()){
                    if (classDataSnapshot.exists()){
                        aClass = classDataSnapshot.getValue(Class.class);
                        if (aClass != null) {
                            classList.add(aClass);

                            int i =0;
                            for (Day day : dayList) {
                                if (day.getDay().equals(classList.get(0).getDay())) {
                                    break;
                                }
                                i++;
                            }
                            spnDay.setSelection(i);

                            int j =0;
                            for (Subject subject : subjectList) {
                                if (subject.getId().equals(classList.get(0).getSubjectId())) {
                                    break;
                                }
                                j++;
                            }
                            spnSubject.setSelection(j);

                            int x =0;
                            for (User user : userList) {
                                if (user.getId().equals(classList.get(0).getTeacherId())) {
                                    break;
                                }
                                x++;
                            }
                            spnTeacher.setSelection(x);

                            int y =0;
                            for (Time time : timeList) {
                                if (time.getTime().equals(classList.get(0).getTime())) {
                                    break;
                                }
                                y++;
                            }
                            spnTime.setSelection(y);

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });

    }

    private void populateDays(HintSpinner spinnerDays){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_DAY);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dayList.clear();
                dayAdapter.clear();
                for (DataSnapshot dayDataSnapshot : dataSnapshot.getChildren()){
                    if (dayDataSnapshot.exists()){
                        day = dayDataSnapshot.getValue(Day.class);
                        if (day != null) {
                            dayAdapter.add(day.getDay());
                            dayList.add(day);

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });

        spinnerDays.setAdapter(dayAdapter);
        spinnerDays.setAdapter(dayAdapter);
    }

    private void populatePlace(HintSpinner spinnerPlace){
        List<String> listPlace = new ArrayList<>();
        listPlace.add("B1");
        listPlace.add("B2");
        listPlace.add("B3");

        ArrayAdapter placeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, listPlace);

        spinnerPlace.setAdapter(placeAdapter);
    }

    private void populateTime(HintSpinner spinnerTime){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_TIME);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                timeList.clear();
                timeAdapter.clear();
                for (DataSnapshot timeDataSnapshot : dataSnapshot.getChildren()){
                    if (timeDataSnapshot.exists()){
                        time = timeDataSnapshot.getValue(Time.class);
                        if (time != null) {
                            timeAdapter.add(time.getTime());
                            timeList.add(time);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });

        spinnerTime.setAdapter(timeAdapter);
    }

    private void populateSubject(final HintSpinner spinnerSubject){

        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_SUBJECT);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                subjectList.clear();
                subjectAdapter.clear();
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        subject = userDataSnapshot.getValue(Subject.class);
                        if (subject != null) {
                            subjectAdapter.add(subject.getName());
                            subjectList.add(subject);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }

        });

        spinnerSubject.setAdapter(subjectAdapter);

    }

    private void populateTeacher(HintSpinner spinnerTeacher){
        DatabaseReference databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_USER);
        databaseReference.orderByChild("user_type").equalTo(Constant.TYPE_TEACHER).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                userAdapter.clear();
                for (DataSnapshot userDataSnapshot : dataSnapshot.getChildren()){
                    if (userDataSnapshot.exists()){
                        user = userDataSnapshot.getValue(User.class);
                        if (user != null) {
                            userAdapter.add(user.getName());
                            userList.add(user);
                        }
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                new GlobalService().showSnackBar(coordinatorLayout, databaseError.getMessage());
            }

        });

        spinnerTeacher.setAdapter(userAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.admin_view_class, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete_class) {
            showDeleteDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminViewClass.this);
        builder.setMessage("Are you sure to delete").setTitle("Delete");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                new ClassService().deleteClass(CLASS_ID, AdminViewClass.this, coordinatorLayout);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
