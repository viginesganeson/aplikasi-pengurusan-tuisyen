package com.ganeson.vigines.aplikasipengurusantuisyen.Services;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.AdminViewClassByDayAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewClassByDayAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Adapters.TeacherViewClassUploadNotesAdapter;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Class;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.List;
import java.util.Locale;

public class ClassService extends GlobalService {

    private DatabaseReference databaseReference;
    private Class aClass;

    public ClassService() {
        this.databaseReference = new FirebaseService().initDatabase(FirebaseService.DBR_CLASS);
    }

    public void addClass(final Context context, String teacherID, String subjectID, String day, String time, String place, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Adding subject...");
        progressDialog.show();
        String id = databaseReference.push().getKey();
        Class aClass = new Class(id, place, time, day, teacherID, subjectID);

        if(id != null) {
            databaseReference.child(id).setValue(aClass)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        showSnackBar(coordinatorLayout, "Subject Added");
                        progressDialog.dismiss();
                        new GlobalService().destroyAct(context);

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showSnackBar(coordinatorLayout, e.getMessage());
                        progressDialog.dismiss();
                    }
                });
        }
    }

    public void getClassByDay(final Context context, String day, final List<Class> classList, final TextView textView, final RecyclerView recyclerView) {
        databaseReference.orderByChild("day").equalTo(day).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                }

                for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()) {
                    aClass = classDataSnapshot.getValue(Class.class);
                    classList.add(aClass);
                }

                recyclerView.setAdapter(new AdminViewClassByDayAdapter(context, classList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void updateClass(final Context context, final String id, final String teacherID, final String subjectID, final String day, final String time, final String place, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Updating class...");
        progressDialog.show();
        Class aClass = new Class(id, place, time, day, teacherID, subjectID);
        databaseReference.child(id).setValue(aClass)
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "Class Updated");
                    progressDialog.dismiss();
                    new GlobalService().destroyAct(context);

                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    showSnackBar(coordinatorLayout, e.getMessage());
                    progressDialog.dismiss();
                }
            });
    }

    public void deleteClass(String id, final Context context, final CoordinatorLayout coordinatorLayout) {
        final ProgressDialog progressDialog = new GlobalService().showProgressDialog(context, "Deleting class...");
        progressDialog.show();
        databaseReference.child(id).removeValue()
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    showSnackBar(coordinatorLayout, "Class Deleted");
                    progressDialog.dismiss();
                    new GlobalService().destroyAct(context);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
    }

    public void getClassForTeacher(final Context context, String teacherId, final List<Class> classList, final TextView textView, final RecyclerView recyclerView) {
        databaseReference.orderByChild("teacherId").equalTo(teacherId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()) {
                        aClass = classDataSnapshot.getValue(Class.class);
                        classList.add(aClass);
                    }
                }

                recyclerView.setAdapter(new TeacherViewClassUploadNotesAdapter(context, classList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getClassByDayForTeacher(final Context context, String teacherId, final String day, final List<Class> classList, final TextView textView, final RecyclerView recyclerView, final CoordinatorLayout coordinatorLayout) {
        databaseReference.orderByChild("teacherId").equalTo(teacherId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    textView.setVisibility(View.VISIBLE);
                }

                if (dataSnapshot.exists()) {
                    textView.setVisibility(View.GONE);
                    for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()) {
                        aClass = classDataSnapshot.getValue(Class.class);
                        if (aClass != null) {
                            if (aClass.getDay().equals(day)){
                                classList.add(aClass);
                            }
                        }
                    }
                }

                if (classList.size() == 0) {
                    textView.setVisibility(View.VISIBLE);
                }

                recyclerView.setAdapter(new TeacherViewClassByDayAdapter(context, classList, coordinatorLayout));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                showSnackBar(coordinatorLayout, databaseError.getMessage());
            }
        });
    }

    public void getClassDashboardForTeacher(String teacherId, final List<Class> classList, final TextView textView) {
        databaseReference.orderByChild("teacherId").equalTo(teacherId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()) {
                        aClass = classDataSnapshot.getValue(Class.class);
                        if (aClass != null) {
                            classList.add(aClass);
                        }
                    }
                }
                if (classList.size() > 0) {
                    textView.setText(String.format(Locale.getDefault(), "%d", classList.size()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void getClassDashboardForAdmin(final List<Class> classList, final TextView textView) {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    for (DataSnapshot classDataSnapshot : dataSnapshot.getChildren()) {
                        aClass = classDataSnapshot.getValue(Class.class);
                        if (aClass != null) {
                            classList.add(aClass);
                        }
                    }
                }

                if (classList.size() > 0) {
                    textView.setText(String.format(Locale.getDefault(), "%d", classList.size()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}