package com.ganeson.vigines.aplikasipengurusantuisyen.Teacher;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ganeson.vigines.aplikasipengurusantuisyen.Admin.AdminViewClassByDay;
import com.ganeson.vigines.aplikasipengurusantuisyen.Models.Note;
import com.ganeson.vigines.aplikasipengurusantuisyen.R;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.ClassService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.FirebaseService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.GlobalService;
import com.ganeson.vigines.aplikasipengurusantuisyen.Services.NoteService;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TeacherViewNotes extends AppCompatActivity {

    private String CLASS_NAME, CLASS_ID;
    private Toolbar toolbar;
    private boolean FILE_STATUS = false;
    private Uri FILE_URI;
    private FloatingActionButton floatingActionButton;
    private CoordinatorLayout coordinatorLayout;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    private List<Note> noteList;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_view_notes);
        CLASS_NAME = getIntent().getStringExtra("CLASS_NAME");
        CLASS_ID = getIntent().getStringExtra("CLASS_ID");

        textView = findViewById(R.id.tv_empty_notes);
        linearLayoutManager = new LinearLayoutManager(TeacherViewNotes.this);
        coordinatorLayout = findViewById(R.id.cl_teacher_view_notes);
        toolbar = findViewById(R.id.tlb_teacher_view_notes);
        floatingActionButton = findViewById(R.id.fab_add_notes);
        recyclerView= findViewById(R.id.rcview_teacher_view_notes);
        recyclerView.setLayoutManager(linearLayoutManager);

        setToolbar(toolbar, CLASS_NAME);
        floatingActionButton.setOnClickListener(this.addNotes);

    }

    private View.OnClickListener addNotes = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setType("application/pdf");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"Select PDF"), 1);

        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadClass();
    }

    private void loadClass(){
        noteList = new ArrayList<>();
        new NoteService().getNotesByClass(TeacherViewNotes.this, CLASS_ID, noteList, recyclerView, textView, coordinatorLayout);
    }

    private void setToolbar(Toolbar toolbar, String day){
        toolbar.setTitle("Add Notes "+ CLASS_NAME);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            FILE_URI = data.getData();
            if(FILE_URI != null) {
                FILE_STATUS = true;
            }

            if (FILE_STATUS){
                String fileName = UUID.randomUUID().toString();
                final StorageReference notesFolder = new FirebaseService().initStorage().child(FirebaseService.STR_NOTES + fileName);
                notesFolder.putFile(FILE_URI)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                notesFolder.getDownloadUrl()
                                        .addOnSuccessListener(new OnSuccessListener<Uri>() {
                                            @Override
                                            public void onSuccess(Uri uri) {
                                                new NoteService().addNote(coordinatorLayout, CLASS_ID, uri.toString());
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                                            }
                                        });
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                new GlobalService().showSnackBar(coordinatorLayout, e.getMessage());
                            }
                        });
            } else {
                new GlobalService().showSnackBar(coordinatorLayout, "No file selected");
            }
        }
    }
}
